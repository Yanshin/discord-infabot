﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EncyclopediaUpdater
{
    class Program
    {
        static class Config
        {
            public static int _season { get; set; }
            public static string _wgkey { get; set; }
            public static string _url { get; set; }
            public static string _realm { get; set; }
            public static string _path { get; set; }
            public static string _filename { get; set; }

            static Config()
            {
                _season = 0;
                _wgkey = "";
                _url = "";
                _realm = "";
                _path = "";
                _filename = "";
            }

            public static void LoadConfig(int season, string wgkey, string url, string realm, string path, string filename)
            {
                _season = season;
                _wgkey = wgkey;
                _url = url;
                _realm = realm;
                _path = path;
                _filename = filename;
            }
        }

        public class ship
        {
            public uint ship_id { get; set; }
            public int tier { get; set; }

            public string name { get; set; }
            public string nameRu { get; set; }
            public string type { get; set; }
            public string nation { get; set; }

            public bool is_premium { get; set; }
            public bool is_special { get; set; }
        }

        public static class ShipList
        {
            static List<ship> shipList;

            // Инициализация статики
            static ShipList()
            {
                shipList = new List<ship>();
            }

            // Метод чистовой записи
            public static void fullRecord(List<ship> ships)
            {
                shipList.Clear();
                shipList.AddRange(ships);
            }

            public static void AddRu(ship shipR)
            {
                shipList.ElementAt(shipList.FindIndex(x => x.ship_id == shipR.ship_id)).nameRu = shipR.name;
            }

            // Метод поиска корабля по его id
            public static ship getShip(uint ship_id)
            {
                ship returnShip = new ship();
                foreach (var item in shipList)
                {
                    if (ship_id == item.ship_id)
                    {
                        returnShip.ship_id = item.ship_id;
                        returnShip.tier = item.tier;
                        returnShip.name = item.name;
                        returnShip.type = item.type;
                        returnShip.nation = item.nation;
                        returnShip.is_premium = item.is_premium;
                        returnShip.is_special = item.is_special;
                    }
                }
                return returnShip;
            }


            public static string GetAllinString()
            {
                string jsoner = "";

                foreach (ship item in shipList)
                {
                    jsoner += JsonConvert.SerializeObject(item, Formatting.Indented);
                }

                // Сериализация не прописывает запятые между элементами массив и не помещает всё в квадратные скобки. Исправляем.
                jsoner = "[" + jsoner.Replace("}{", "},{") + "]";
                return jsoner;
            }
        }


        static void Main(string[] args)
        {
            // Загрузка конфиг-файла
            loadConfig();

            // Создание каталога и блок файла
            createBlock();

            // Получение списка кланов
            shipsLoading();

            // Запись в файл
            createShipsData();

            // Удаление блок файла
            deleteBlock();
        }

        private static void createShipsData()
        {
            // Создание файла json с записью в него содержимого клан-листа, используя метод GetAllinString()
            try
            {
                string shortpath = "Data\\";
                File.WriteAllText(shortpath + "ships_info.json", ShipList.GetAllinString());

                Console.WriteLine("Ships file created");
            }
            catch (Exception e)
            {
                Console.WriteLine("Can't create json-file:");
                Console.WriteLine(e);
            }
        }

        private static void shipsLoading()
        {
            List<ship> loadedShips = new List<ship>();

            using (WebClient webClient = new System.Net.WebClient())
            {
                // Инициализация Вэб клиента
                WebClient n = new WebClient();
                WebClient m = new WebClient();
                n.Encoding = System.Text.Encoding.UTF8;
                m.Encoding = System.Text.Encoding.UTF8;

                string shipsJSONtext = "";
                string shipsJSONtextRu = "";
                shipsJSONtext = n.DownloadString("https://api.worldofwarships.com/wows/encyclopedia/ships/?application_id=b385059f5abf2cba21f1da8d36827ad9");
                shipsJSONtextRu = m.DownloadString("https://api.worldofwarships.com/wows/encyclopedia/ships/?application_id=b385059f5abf2cba21f1da8d36827ad9&language=ru&fields=name%2C+ship_id");

                
                JObject jships = JObject.Parse(shipsJSONtext);
                JObject jshipsRu = JObject.Parse(shipsJSONtextRu);

                int page_total = (int)jships["meta"]["page_total"];

                JEnumerable<JToken> shlist = jships["data"].Children();
                JEnumerable<JToken> shlistRu = jshipsRu["data"].Children();

                for (int i=0; i < shlist.Count(); i++)
                {
                    loadedShips.Add(new ship
                    {
                        ship_id = (uint)shlist.ElementAt(i).First["ship_id"],
                        tier = (int)shlist.ElementAt(i).First["tier"],
                        name = (string)shlist.ElementAt(i).First["name"],
                        nameRu = (string)shlistRu.ElementAt(i).First["name"],
                        type = (string)shlist.ElementAt(i).First["type"],
                        nation = (string)shlist.ElementAt(i).First["nation"],
                        is_premium = (bool)shlist.ElementAt(i).First["is_premium"],
                        is_special = (bool)shlist.ElementAt(i).First["is_special"],
                    });
                }
                
                
                for (int i=2; i <= page_total; i++)
                {
                    Console.WriteLine($"Starting load page {i}");

                    shipsJSONtext = n.DownloadString("https://api.worldofwarships.com/wows/encyclopedia/ships/?application_id=b385059f5abf2cba21f1da8d36827ad9&page_no=" + i);
                    shipsJSONtextRu = m.DownloadString("https://api.worldofwarships.com/wows/encyclopedia/ships/?application_id=b385059f5abf2cba21f1da8d36827ad9&language=ru&fields=name%2C+ship_id&page_no=" + i);

                    jships = JObject.Parse(shipsJSONtext);
                    jshipsRu = JObject.Parse(shipsJSONtextRu);

                    shlist = jships["data"].Children();
                    shlistRu = jshipsRu["data"].Children();

                    for (int j = 0; j < shlist.Count(); j++)
                    {
                        loadedShips.Add(new ship
                        {
                            ship_id = (uint)shlist.ElementAt(j).First["ship_id"],
                            tier = (int)shlist.ElementAt(j).First["tier"],
                            name = (string)shlist.ElementAt(j).First["name"],
                            nameRu = (string)shlistRu.ElementAt(j).First["name"],
                            type = (string)shlist.ElementAt(j).First["type"],
                            nation = (string)shlist.ElementAt(j).First["nation"],
                            is_premium = (bool)shlist.ElementAt(j).First["is_premium"],
                            is_special = (bool)shlist.ElementAt(j).First["is_special"],
                        });
                    }
                }
            }

            //https://api.worldofwarships.ru/wows/encyclopedia/ships/?application_id=b385059f5abf2cba21f1da8d36827ad9
            ShipList.fullRecord(loadedShips);
        }

        private static void deleteBlock()
        {
            // Удаляем block-file
            try
            {
                File.Delete("Data\\blocker.txt");
            }
            catch (Exception e)
            {
                Console.WriteLine("Can't delete block-file:");
                Console.WriteLine(e);
            }
        }

        private static void createBlock()
        {
            // Создание каталога с именем path + seasonpath
            try
            {
                Console.WriteLine($"Creating block-file in Data");

                System.IO.Directory.CreateDirectory("Data");

                File.WriteAllText("Data\\blocker.txt", "");


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void loadConfig()
        {
            string xmlpath = "Config\\";
            string xmlFile = "config.xml";

            // Проверка существования файла
            if (File.Exists(xmlpath + xmlFile))
            {
                Console.WriteLine($"Loading config from {xmlpath + xmlFile}");
                string xml;
                try
                {
                    xml = File.ReadAllText(xmlpath + xmlFile);
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(xml);
                    Console.WriteLine("Config loaded from file");

                    string jsonText = JsonConvert.SerializeXmlNode(doc);
                    JObject config = JObject.Parse(jsonText);

                    Console.WriteLine();

                    string wgkey = (string)config["DiscordServer"]["config"]["wgapi"]["wgkey"];
                    string url = (string)config["DiscordServer"]["config"]["wgapi"]["url"];
                    string realm = (string)config["DiscordServer"]["config"]["wgapi"]["realm"];
                    string path = (string)config["DiscordServer"]["data"]["clans"]["path"];
                    string filename = (string)config["DiscordServer"]["data"]["clans"]["filename"];
                    int currentSeason = (int)config["DiscordServer"]["data"]["clans"]["seasons"];

                    Config.LoadConfig(currentSeason, wgkey, url, realm, path, filename);


                }
                catch (Exception e)
                {
                    Console.WriteLine("Can't load config from file");
                    Console.WriteLine(e);
                }


            }
            else
            {
                Console.WriteLine($"Can't find file {xmlFile}");
            }

        }
    }
}
