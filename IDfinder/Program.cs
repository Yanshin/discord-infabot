﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace IDfinder
{
    class Program
    {
        public static char[] _symbols = { '_', '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        public static uint _usersCount = 0;
        public static char _nextChar = '_';

        public static class UserList
        {
            public static int saveroll { get; set; }

            public class User
            {
                public uint account_id { get; set; }
                public string nickname { get; set; }
            }

            public static List<User> _userlist;

            static UserList()
            {
                _userlist = new List<User>();
                saveroll = 500;
            }

            public static void AddUser(uint account_id, string nickname)
            {
                User newUser = new User();

                newUser.account_id = account_id;
                newUser.nickname = nickname;

                _userlist.Add(newUser);

                if (_userlist.Count >= saveroll)
                {
                    // Запись в файл
                    if (!File.Exists("list.txt"))
                    {
                        File.CreateText("list.txt");
                    }
                    string toFile = "";
                    foreach (User item in _userlist)
                    {
                        toFile += $"{item.account_id},{item.nickname};\r\n";
                    }

                    File.AppendAllText("list.txt", toFile);
                    _userlist.Clear();
                }
            }

            public static void SetItSave(int number)
            {
                saveroll = number;
            }
        }

        public static string _apiKey = "b385059f5abf2cba21f1da8d36827ad9";

        static void Main(string[] args)
        {

            Console.WriteLine("Program started with apikey " + _apiKey);

            if (args.Length != 0)
            {
                UserList.SetItSave( Int32.Parse(args[0]) );
            }
            string lastNick = loadConfig();

            var webclient = new WebClient();

            //Проверяем наличие выгруженного списка
            if (lastNick.Length != 0 )
            {
                // Вытаскиваем ник из последней строки
                lastNick = lastNick.Substring( (lastNick.IndexOf(',') + 1), (lastNick.IndexOf(';') - 1) - (lastNick.IndexOf(',') ));

                Console.WriteLine("Starting find prefix");
                string lastPrefix = (recursiveLastPrefix(webclient, lastNick) + _nextChar).ToLower();
                Console.WriteLine($"Next prefix: {lastPrefix}\nTrying to start from it");

                //Console.ReadLine();

                recursivePlayerSearch(webclient, "", lastPrefix);
            } else
            {
                recursivePlayerSearch(webclient, "");
            }
                      
            Console.ReadLine();
        }

        // Функция поиска последнего использованного префикса имени
        private static string recursiveLastPrefix(WebClient webclient, string lastNick)
        {
            using (webclient)
            {
                if (lastNick.Length > 3 && lastNick != "player_" && lastNick != "player_0")
                {
                    var onlineJSONtext = webclient.DownloadString("https://api.worldoftanks.eu/wgn/account/list/" +
                    "?application_id=" + _apiKey +
                    "&search=" + lastNick + "&game=wows&type=startswith");

                    JObject rss = JObject.Parse(onlineJSONtext);

                    if ((int)rss["meta"]["count"] < 100) // Если результат больше 100, идём вверх
                    {
                        _nextChar = lastNick.ToLower().Last();
                        Console.WriteLine("Going up: " + lastNick);

                        lastNick = recursiveLastPrefix(webclient, lastNick.Substring(0, lastNick.Length - 1));
                    }

                    else // Смещаем последнюю букву префикса
                    {
                        int nextChar = Array.IndexOf(_symbols, _nextChar);

                        if (nextChar < (_symbols.Length - 1))
                        {
                            _nextChar = _symbols[nextChar + 1];
                        }
                    }

                }
                else
                {
                    int nextChar = Array.IndexOf(_symbols, _nextChar);

                    if (nextChar < (_symbols.Length - 1))
                    {
                        _nextChar = _symbols[nextChar + 1];
                    }
                }  
            }
            return lastNick;
        }

        // Функция поиска игроков начиная с lastPrefix
        private static async void recursivePlayerSearch(WebClient webclient, string prefix, string lastPrefix = "________________________")
        {
            //Console.WriteLine(prefix);
            int depth = prefix.Length;
            int lastChar;


            bool isBreaker = false;

            // Проверка, что мы уже ушли от букв последнего ника
            for (int i = 0; i < prefix.Length; i++)
            {
                if (prefix[i] != lastPrefix[i])
                {
                    isBreaker = true;
                    break;
                }
            }

            if ( lastPrefix.Length > depth && !isBreaker)
            {
                lastChar = Array.IndexOf(_symbols, lastPrefix[depth]);
            } else
            {
                lastChar = 0;
            }
            
            
            if (prefix.Length <= 2 ) // Добавляем символы к поиску, если их меньше 3 
            {
                for (int i = lastChar; i < _symbols.Length; i++)
                {
                    recursivePlayerSearch(webclient, prefix + _symbols[i], lastPrefix);
                }

            } else if (prefix == "player_" || prefix == "player_0") // и проверяем, что это не player_ или player_[цифра]
            {
                for (int i = lastChar; i < 11; i++)
                {
                    recursivePlayerSearch(webclient, prefix + _symbols[i], lastPrefix);
                }
                return;
            }
            else // Ищем игроков
            {
                using (webclient)
                {
                    var onlineJSONtext = webclient.DownloadString("https://api.worldoftanks.eu/wgn/account/list/" +
                        "?application_id=" + _apiKey +
                        "&search=" + prefix + "&game=wows&type=startswith");

                    JObject rss = JObject.Parse(onlineJSONtext); // десериализуем JSON

                    if ( (string)rss["status"] == "error") // Если ошибка, поднимаемся на уровень выше
                    {
                        return;
                    }
                    if ((int)rss["meta"]["count"] == 100) // Если не все ники поместились
                    {
                        // Тут можно ускорить

                        for (int i=lastChar; i < _symbols.Length; i++)
                        {
                            recursivePlayerSearch(webclient, prefix + _symbols[i], lastPrefix);
                        }
                    }
                    else // Все ники подстроки в выдаче. Записываем
                    {
                        
                        JArray nicks = JArray.FromObject(rss["data"]);
                        
                        if (nicks.Count != 0)
                        {
                            _usersCount += (uint)nicks.Count;

                            Console.WriteLine($"{prefix,-10} finded nicknames:{nicks.Count,-10}");

                            InputTopConsoleLine($"Current depth:{prefix.Length,-2}| Total nicknames:{ _usersCount,-8} ({calcPercent(prefix[0],prefix[1], prefix[2])}%)");

                            foreach (var item in nicks)
                            {
                                // Метод записи
                                uint account_id = (uint)item["account_id"];
                                string nickname = (string)item["nickname"];
                                UserList.AddUser(account_id, nickname);
                                //Console.WriteLine(item);
                            }  
                        }
                        return;
                    }
                }
            } 
        }

        // Функция подсчёта процента от всех комбинаций первых двух символов
        static double calcPercent(char v1, char v2, char v3)
        {
            int symbols_num = _symbols.Length;
            // порядок первого символа * число символов + порядок второго символа
            double percent = (
                (
                Array.IndexOf(_symbols, v1) * symbols_num * symbols_num)
                + (Array.IndexOf(_symbols, v2) * symbols_num)
                + Array.IndexOf(_symbols, v3)
                )
                / 
                (symbols_num * symbols_num * symbols_num)
                * 100;
            
            return percent;
        }

        // Функция красивого вывода общего статуса
        static void InputTopConsoleLine(string topstring = "")
        {
            int currentLineCursor = Console.CursorTop;

            // затираем строку пробелами
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));

            // Пишем новую информацию
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(topstring);
            Console.SetCursorPosition(0, currentLineCursor);
        }

        static string loadConfig()
        {
            string xmlpath = "Config\\";
            string xmlFile = "config.txt";

            // Проверка существования файла
            if (File.Exists(xmlpath + xmlFile))
            {
                Console.WriteLine("Finded conf");
                
            }
            if (File.Exists("list.txt"))
            {
                if (File.ReadAllLines("list.txt").Length > 0)
                {
                    _usersCount = (uint)File.ReadAllLines("list.txt").Length;
                    Console.WriteLine($"Finded {_usersCount} in file.");
                    return File.ReadAllLines("list.txt").Last();
                }
            } 
            return "";
        }
    }
}