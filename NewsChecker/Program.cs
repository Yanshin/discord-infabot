﻿using CodeHollow.FeedReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsChecker
{
    class Program
    {

#if DEBUG
        private static bool DebugOnlyCode = true;
#endif

        static void Main(string[] args)
        {
            //https://worldofwarships.ru/ru/rss/news/


            var feed = FeedReader.Read("https://worldofwarships.ru/ru/rss/news/");

            // ...

            Console.WriteLine("last item is " + feed.Items.ElementAt(0).PublishingDate);

            DateTime lastDate = DateTime.Parse(feed.Items.ElementAt(0).PublishingDate.ToString());

            Console.WriteLine(lastDate);

            foreach (var item in feed.Items)
            {
                //Console.WriteLine(item.Title + " \n " + item.Link);
            }

            Console.ReadLine();
        }
    }
}
