﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClanUpdater
{

    class Program
    {
        #if DEBUG
                private static bool DebugOnlyCode = true;
        #endif

        static class Config
        {
            public static int _season { get; set; }
            public static string _wgkey { get; set; }
            public static string _url { get; set; }
            public static string _realm { get; set; }
            public static string _path { get; set; }
            public static string _filename{ get; set; }
            public static Season _currentSeason { get; set; }

            static Config()
            {
                _season = 2;
                _wgkey = "";
                _url = "";
                _realm = "";
                _path = "";
                _filename = "";
                _currentSeason = new Season();
            }

            public static void LoadConfig(int season, string wgkey, string url, string realm, string path, string filename, Season currentSeason)
            {
                _season = season;
                _wgkey = wgkey;
                _url = url;
                _realm = realm;
                _path = path;
                _filename = filename;
                _currentSeason = currentSeason;
            }
        }

        class Season
        {
            public int min_league { get; set; }
            public int max_league { get; set; }
            public int min_division { get; set; }
            public int max_division { get; set; }
            public string path { get; set; }
        }

        public static class Progress
        {
            static List<DivProgress> ProgressList;
            static string PrefixText;
            static bool DrawFlag;

            static Progress()
            {
                ProgressList = new List<DivProgress>();
            }

            public static void AddText(string text)
            {
                PrefixText += text + "\n";
                Console.WriteLine(text);
            }

            public static void CountDiv(int league, int division)
            {
                int index = ProgressList.FindIndex(o => o.league == league && o.division == division);

                if (index != -1)
                {
                    ProgressList[index].nums++;
                } else
                {
                    DivProgress temp = new DivProgress();
                    temp.league = league;
                    temp.division = division;
                    temp.nums = 1;
                    temp.isStarted = true;
                    ProgressList.Add(temp);
                    Order();
                }
            }

            public static void CompleteDiv(int league, int division)
            {
                int index = ProgressList.FindIndex(o => o.league == league && o.division == division);
                ProgressList[index].IsComplete = true;

                DrawProgress();
            }

            public static void Order()
            {
                ProgressList = ProgressList.OrderBy(o => o.league).ThenBy(o => o.division).ToList();
            }

            public static void DrawProgress()
            {
                if (!DrawFlag)
                {
                    DrawFlag = true;

                    Console.Clear();

                    string output = PrefixText + "\nProgress:";

                    int lastLeague = -1;
                    foreach (DivProgress item in ProgressList)
                    {
                        if (lastLeague != item.league)
                        {
                            lastLeague = item.league;
                            output += "\nLeague: " + item.league + "\t";
                        }
                        else
                        {
                            output += "\t\t";
                        }

                        output += "Division: " + item.division + "\tClans:" + item.nums + " ";
                        if (item.IsComplete)
                            output += "\t Ok";
                        output += "\n";
                        Console.Write(output);
                        output = "";
                    }

                    DrawFlag = false;
                }
                
            }
        }

        public class DivProgress
        {
            public int league { get; set; }
            public int division { get; set; }
            public int nums { get; set; }
            public bool isStarted { get; set; }
            public bool IsComplete { get; set; }
        }

        //Конструктор класса Клан
        public class cln
        {
            public uint cID { get; set; }
            public ushort cTeam { get; set; }
            public string cTag { get; set; }
            public string cName { get; set; }
            public string cRealm { get; set; }
            public ushort cLeague { get; set; }
            public ushort cDivision { get; set; }
            public ushort cRank { get; set; }
            public ushort cGlobalRank { get; set; }
            public ushort cRating { get; set; }
            public ushort cPR { get; set; }
            public ushort cBattles { get; set; }
        }

        //Конструктор статичного класса клан-листа
        public static class ClanList
        {
            public static List<cln> _cln;

            // Инициализация статичного класса
            static ClanList()
            {
                _cln = new List<cln>();
            }

            // Метод записи в лист класса одного клана
            public static void RecordByOne(cln newest)
            {
                _cln.Add(newest);
                //Console.WriteLine($"[{newest.cTag}] {newest.cRank}" );
            }

            // Метод записи в лист класса всего диапазона начисто
            public static void ClearRecord(List<cln> newlist)
            {
                if (newlist.Count != 0)
                {
                    _cln.Clear();
                }
                _cln.AddRange(newlist);

            }

            public static void Ordering()
            {
                Console.WriteLine("Ordering " + _cln.Count() + " clans");
                _cln = _cln.OrderBy(x => x.cLeague).ThenBy(x => x.cDivision).ThenByDescending(x => x.cRating).ToList();

                ushort globalRank = 1;

                foreach (cln item in _cln)
                {
                    item.cGlobalRank = globalRank;
                    globalRank++;
                }
                Console.WriteLine("Order complete");
            }

            // Метод выдачи счётчика листа
            public static int Count()
            {
                return _cln.Count;
            }

            // Метод перевода листа в строку формата json
            public static string GetAllinString()
            {
                string jsoner = "";

                foreach (cln item in _cln)
                {
                    jsoner += JsonConvert.SerializeObject(item, Formatting.Indented);
                }

                // Сериализация не прописывает запятые между элементами массив и не помещает всё в квадратные скобки. Исправляем.
                jsoner = "[" + jsoner.Replace("}{", "},{") + "]";
                return jsoner;
            }
        }

        static void Main(string[] args)
        {
        #if DEBUG
                    if (DebugOnlyCode)
                    {
                        Progress.AddText("Debug mode on");
                        Progress.AddText("Additional arg is: " + args[0]);
                    }
        #endif



            string realm = "ru";

            if ( args.Contains("eu"))
            {
                realm = "eu";
            }
            if (args.Contains("na"))
            {
                realm = "com";
            }
            if (args.Contains("asia"))
            {
                realm = "asia";
            }

#if DEBUG
            if (DebugOnlyCode)
            {
                Progress.AddText("Realm is " + realm);
            }
#endif


            // Загрузка конфиг-файла
            LoadConfig();

            // Создание каталога и блок файла
            // Утратило актуальность
            //createBlock();

            // Получение списка кланов
            //clanLoading(Config._season, realm);


            GetClanList(Config._season, realm);

            ClanList.Ordering();

            // Запись в файл
            createClanData(realm);

            // Удаление блок файла
            deleteBlock();
        }

        private static void deleteBlock()
        {
            // Удаляем block-file
            try
            {
                File.Delete(Config._path + "\\" + Config._currentSeason.path + "\\blocker.txt");
            }
            catch (Exception e)
            {
                Progress.AddText("Can't delete block-file:");
                Progress.AddText(e.ToString());
            }
        }

        private static void createClanData(string realm)
        {
            // Создание файла json с записью в него содержимого клан-листа, используя метод GetAllinString()
            try
            {
                string shortpath = Config._path + "\\" + Config._currentSeason.path + "\\";

                if (File.Exists(shortpath + realm + Config._filename))
                {
                    
                    if (File.Exists(shortpath + realm + "backup_" + Config._filename))
                    {
                        File.Delete(shortpath + realm + "backup_" + Config._filename);
                    }
                    // Создаём резервную копию
                    File.Copy(shortpath + realm + Config._filename, shortpath + realm + "backup_" + Config._filename);

                    // Удаляем laststats копию
                    File.Delete(shortpath + realm + Config._filename);

                }

                // Создаём laststats
                File.WriteAllText(shortpath + realm + Config._filename, ClanList.GetAllinString());
                System.Threading.Thread.Sleep(100);
                Progress.AddText("Json file created");

                File.Copy(shortpath + realm + Config._filename, shortpath + realm + DateTime.Today.ToString("yyyy-MM-dd") + ".json");
                System.Threading.Thread.Sleep(100);
                Progress.AddText("Json file with date created");


                Progress.AddText("Backup updated");
                System.Threading.Thread.Sleep(100);
            }
            catch (Exception e)
            {
                Progress.AddText("Can't create json-file:");
                Progress.AddText(e.ToString());
            }
        }

        private static void clanLoading(int season, string realm)
        {
            // Переменные для выхода из циклов
            int LastLeague = Config._currentSeason.max_league;
            int LastDivision = Config._currentSeason.max_division;
            // Переменная глобального порядкового номера
            ushort globalRank = 1;
            ushort groupRank = 1;

            Progress.AddText("Starting load season " + Config._season);
            // Цикл лиг от 0 до LastLeague
            for (int league = Config._currentSeason.min_league; league <= LastLeague; league++)
            {
                Progress.AddText($"Starting fetch league {league} ");
                groupRank = 1;

                // Цикл групп от 0 до LastDivision
                for (int division = Config._currentSeason.min_division; division <= LastDivision; division++)
                {
                    Progress.AddText($"... division {division} ");

                    // Локальная переменная для движения до конца группы
                    uint last_id;

                    using (WebClient webClient = new System.Net.WebClient())
                    {
                        // Инициализация Вэб клиента
                        WebClient n = new WebClient();
                        n.Encoding = System.Text.Encoding.UTF8;
                        // Запись в переменную ответа на запрос (головной) топ кланов текущей лиги и группы (выдаёт не более 25 кланов)  
                        string querry = Config._url + realm + "/clans/wows/ladder/api/structure/" +
                            "?league=" + league +
                            "&division=" + division +
                            "&season=" + season +
                            "&is_global=false";

                        Progress.AddText($"Querry: {querry} ");

                        var clanJSONtext = n.DownloadString(querry);

                        
                        if (clanJSONtext.Contains("{\"errors\":"))
                        {
                            return;
                        }
                        

                        // Превращаем ответ в json-массив
                        JArray rss = JArray.Parse(clanJSONtext);

                        //Progress.AddText(rss.Count);
                        if (rss.Count != 0)
                        {
                            // Цикл json-массива от первого до последнего значения
                            for (int i = 0; i < rss.Count; i++)
                            {
                                last_id = 0;
                                
                                //Progress.AddText( $"{(string)rss[i]["tag"]} {(groupRank + 1)} {globalRank}");
                                // Запись в клан-лист через метод RecordByOne() клана с порядковым номером i
                                ClanList.RecordByOne(new cln()
                                {
                                    cName = (string)rss[i]["name"],
                                    cTag = (string)rss[i]["tag"],
                                    cBattles = (ushort)rss[i]["battles_count"],
                                    cRank = (ushort)rss[i]["rank"],
                                    cDivision = (ushort)rss[i]["division"],
                                    cLeague = (ushort)rss[i]["league"],
                                    cID = (uint)rss[i]["id"],
                                    cGlobalRank = globalRank++,
                                    cRating = (ushort)rss[i]["division_rating"],
                                    cPR = (ushort)rss[i]["public_rating"],
                                    cRealm = realm
                                });


                                // Проверка на последний клан в ответе на головной запрос
                                if (i == rss.Count - 1)
                                {
                                    // Запись ID этого клана в переменную
                                    last_id = (uint)rss[i]["id"];
                                    //Progress.AddText("Last id is: " + last_id);
                                    groupRank = (ushort)rss[i]["rank"];
                                    // Создание и инициализация переменной-флага на последний клан в подзапросе
                                    bool isntlast = true;

                                    // Цикл пока не будет найден последний клан в текущей группе
                                    while (isntlast)
                                    {
                                        // Запись в переменную ответа на под-запрос позиции клана в его группе (обычно выдаёт первые 3 клана в группе, вышестоящий клан, текущий клан и до 4 следующих) 
                                        clanJSONtext = n.DownloadString("https://clans.worldofwarships." + realm + "/clans/wows/ladder/api/structure/" +
                                            "?clan_id=" + last_id +
                                            "&season=" + season +
                                            "&is_global=false");

                                        // Превращаем ответ во второй json-массив
                                        JArray rss2 = JArray.Parse(clanJSONtext);

                                        // Создание и инициализация переменной позиции клана в подзапросе (он не всегда будет 5-м)
                                        int recounter = 0;


                                        // Иначе ищем клан из подзапросса во всём массиве
                                        for (int j = 0; j < rss2.Count; j++)
                                        {
                                            if ((int)rss2[j]["id"] == last_id)
                                            {
                                                // Дополнительный клан j
                                                recounter = j;
                                                //Progress.AddText("in subq clan is " + recounter);
                                            }
                                        }

                                        // Инкремент чтобы повторно не записать тот же клан
                                        recounter++;

                                        // Проверка, есть ли в подзапросе следующие за искомым кланом другие кланы 
                                        if (rss2.Count > recounter)
                                        {
                                            // Цикл от следующего после искомого клана до последнего в подзапросе
                                            for (int j = recounter; j < rss2.Count; j++)
                                            {
                                                //Progress.AddText($"{(string)rss2[i]["tag"]} {(groupRank + 1)} {globalRank}");

                                                // Запись клана j в клан-лист методом RecordByOne()
                                                ClanList.RecordByOne(new cln()
                                                {
                                                    cName = (string)rss2[j]["name"],
                                                    cTag = (string)rss2[j]["tag"],
                                                    cBattles = (ushort)rss2[j]["battles_count"],
                                                    cRank = ++groupRank,
                                                    //cRank = (ushort)rss2[j]["rank"],
                                                    cDivision = (ushort)rss2[j]["division"],
                                                    cLeague = (ushort)rss2[j]["league"],
                                                    cID = (uint)rss2[j]["id"],
                                                    cGlobalRank = globalRank++,
                                                    cRating = (ushort)rss2[j]["division_rating"],
                                                    cPR = (ushort)rss[i]["public_rating"],
                                                    cRealm = realm
                                                });

                                                // Перезапись в last_id последнего записанного клана. Повторяется каждую итерацию цикла.
                                                last_id = (uint)rss2[j]["id"];
                                            }
                                        }
                                        else
                                        {
                                            // В подзапросе за искомым кланом ничего нет. Устанавливаем флаг на выход из цикла
                                            isntlast = false;
                                        }
                                        //Console.ReadLine();
                                    }
                                }
                            }
                        }
                        
                    }

                    // Выходим из цикла групп, если находимся в высшей лиге (т.к там одна группа)
                    if (league == 0)
                    {
                        break;
                    }
                }
            }
            //Progress.AddText($"There is {ClanList.Count()} clans total");
        }

        private static void GetClanList(int season = 2, string realm = "ru" )
        {
            List<cln> currentList = new List<cln>();

            List<cln> league0 = new List<cln>();
            List<cln> league1 = new List<cln>();
            List<cln> league2 = new List<cln>();
            List<cln> league3 = new List<cln>();
            List<cln> league4 = new List<cln>();

            Task taskL4 = Task.Factory.StartNew(() => league4 = GetLeague(season, realm, 4));
            Task taskL3 = Task.Factory.StartNew(() => league3 = GetLeague(season, realm, 3));
            Task taskL2 = Task.Factory.StartNew(() => league2 = GetLeague(season, realm, 2));
            Task taskL1 = Task.Factory.StartNew(() => league1 = GetLeague(season, realm, 1));
            Task taskL0 = Task.Factory.StartNew(() => league0 = GetLeague(season, realm, 0));


            Task.WaitAll(taskL0, taskL1, taskL2, taskL3, taskL4);
            currentList.AddRange(league1);
            currentList.AddRange(league2);
            currentList.AddRange(league3);
            currentList.AddRange(league4);

            ClanList.ClearRecord(currentList);
        }

        private static List<cln> GetLeague(int season = 2, string realm = "ru", int league = 0)
        {
#if DEBUG
            if (DebugOnlyCode)
            {
                //Progress.AddText("Starting GetLeague() with arg " + league);
            }
#endif


            List<cln> currentList = new List<cln>();

            if (league == 0)
            {
                currentList.AddRange(GetDivision(season, realm, 0, 1));

            } else
            {
                List<cln> division1 = new List<cln>();
                List<cln> division2 = new List<cln>();
                List<cln> division3 = new List<cln>();

                Task taskD2 = Task.Factory.StartNew(() => division2 = GetDivision(season, realm, league, 2));
                Task taskD3 = Task.Factory.StartNew(() => division3 = GetDivision(season, realm, league, 3));
                Task taskD1 = Task.Factory.StartNew(() => division1 = GetDivision(season, realm, league, 1));

                Task.WaitAll(taskD1, taskD2, taskD3);

                currentList.AddRange(division1);
                currentList.AddRange(division2);
                currentList.AddRange(division3);
            }
            //Progress.AddText($"League {league} loaded.");

            return currentList;
        }

        private static List<cln> GetDivision(int season = 2, string realm="ru", int league = 0, int division = 1)
        {
#if DEBUG
            if (DebugOnlyCode)
            {
                //Progress.AddText("Starting GetDivision() with league " + league + " division" + division);
            }
#endif

            List<cln> currentList = new List<cln>();

            //Progress.AddText($"... division {division} ");

            // Локальная переменная для движения до конца группы
            uint last_id;
            ushort groupRank = 1;

            using (WebClient webClient = new System.Net.WebClient())
            {
                // Инициализация Вэб клиента
                WebClient n = new WebClient();
                n.Encoding = System.Text.Encoding.UTF8;
                // Запись в переменную ответа на запрос (головной) топ кланов текущей лиги и группы (выдаёт не более 25 кланов)  
                string querry = Config._url + realm + "/clans/wows/ladder/api/structure/" +
                    "?league=" + league +
                    "&division=" + division +
                    "&season=" + season +
                    "&is_global=false";

                var clanJSONtext = n.DownloadString(querry);


                if (clanJSONtext.Contains("{\"errors\":"))
                {
                    return currentList;
                }


                // Превращаем ответ в json-массив
                JArray rss = JArray.Parse(clanJSONtext);

                //Progress.AddText(rss.Count);
                if (rss.Count != 0)
                {
                    // Цикл json-массива от первого до последнего значения
                    for (int i = 0; i < rss.Count; i++)
                    {
                        last_id = 0;

#if DEBUG
                        if (DebugOnlyCode)
                        {
                            //Progress.AddText($"{(string)rss[i]["tag"]} {(groupRank + 1)}");
                        }
#endif

                        if ((ushort)rss[i]["battles_count"] == 0)
                            break;

                        // Запись в клан-лист через метод RecordByOne() клана с порядковым номером i
                        ClanList.RecordByOne(new cln()
                        {
                            cName = (string)rss[i]["name"],
                            cTag = (string)rss[i]["tag"],
                            cTeam = (ushort)rss[i]["leading_team_number"],
                            cBattles = (ushort)rss[i]["battles_count"],
                            cRank = (ushort)rss[i]["rank"],
                            cDivision = (ushort)rss[i]["division"],
                            cLeague = (ushort)rss[i]["league"],
                            cID = (uint)rss[i]["id"],
                            cRating = (ushort)rss[i]["division_rating"],
                            cPR = (ushort)rss[i]["public_rating"],
                            cRealm = realm
                        });

                        Progress.CountDiv(league, division);

                        // Проверка на последний клан в ответе на головной запрос
                        if (i == rss.Count - 1)
                        {
                            // Запись ID этого клана в переменную
                            last_id = (uint)rss[i]["id"];
                            //Progress.AddText("Last id is: " + last_id);
                            groupRank = (ushort)rss[i]["rank"];
                            // Создание и инициализация переменной-флага на последний клан в подзапросе
                            bool isntlast = true;

                            // Цикл пока не будет найден последний клан в текущей группе
                            while (isntlast)
                            {
                                // Запись в переменную ответа на под-запрос позиции клана в его группе (обычно выдаёт первые 3 клана в группе, вышестоящий клан, текущий клан и до 4 следующих) 
                                clanJSONtext = n.DownloadString("https://clans.worldofwarships." + realm + "/clans/wows/ladder/api/structure/" +
                                    "?clan_id=" + last_id +
                                    "&season=" + season +
                                    "&is_global=false");

                                Progress.DrawProgress();

                                // Превращаем ответ во второй json-массив
                                JArray rss2 = JArray.Parse(clanJSONtext);

                                // Создание и инициализация переменной позиции клана в подзапросе (он не всегда будет 5-м)
                                int recounter = 0;


                                // Иначе ищем клан из подзапросса во всём массиве
                                for (int j = 0; j < rss2.Count; j++)
                                {
                                    if ((int)rss2[j]["id"] == last_id)
                                    {
                                        // Дополнительный клан j
                                        recounter = j;
                                        //Progress.AddText("in subq clan is " + recounter);
                                    }
                                }

                                // Инкремент чтобы повторно не записать тот же клан
                                recounter++;

                                // Проверка, есть ли в подзапросе следующие за искомым кланом другие кланы 
                                if (rss2.Count > recounter)
                                {
                                    // Цикл от следующего после искомого клана до последнего в подзапросе
                                    for (int j = recounter; j < rss2.Count; j++)
                                    {
                                        //Progress.AddText($"{(string)rss2[i]["tag"]} {(groupRank + 1)} {globalRank}");
                                        if ((ushort)rss2[j]["battles_count"] == 0)
                                        {
                                            Progress.CompleteDiv(league, division);
                                            isntlast = false;

                                        } else
                                        {
                                            // Запись клана j в клан-лист методом RecordByOne()
                                            ClanList.RecordByOne(new cln()
                                            {
                                                cName = (string)rss2[j]["name"],
                                                cTag = (string)rss2[j]["tag"],
                                                cTeam = (ushort)rss[i]["leading_team_number"],
                                                cBattles = (ushort)rss2[j]["battles_count"],
                                                cRank = ++groupRank,
                                                //cRank = (ushort)rss2[j]["rank"],
                                                cLeague = (ushort)rss2[j]["league"],
                                                cDivision = (ushort)rss2[j]["division"],
                                                cID = (uint)rss2[j]["id"],
                                                cRating = (ushort)rss2[j]["division_rating"],
                                                cPR = (ushort)rss[i]["public_rating"],
                                                cRealm = realm
                                            });

                                            Progress.CountDiv(league, division);
                                            // Перезапись в last_id последнего записанного клана. Повторяется каждую итерацию цикла.
                                            last_id = (uint)rss2[j]["id"];
                                        }
                                            
                                        
                                    }
                                }
                                else
                                {
                                    // В подзапросе за искомым кланом ничего нет. Устанавливаем флаг на выход из цикла
                                    isntlast = false;
                                    Progress.CompleteDiv(league, division);

                                }
                                //Console.ReadLine();
                            }
                        }
                    }
                }
            }

            //Progress.AddText($"League {league} Division {division} loaded. There are {groupRank-1} clans.");
            return currentList;
        }

        //private static void createBlock()
        //{
        //    // Создание каталога с именем path + seasonpath
        //    try
        //    {
        //        Progress.AddText($"Creating block-file in {Config._path + "\\" + Config._currentSeason.path}");

        //        System.IO.Directory.CreateDirectory(Config._path + "\\" + Config._currentSeason.path);

        //        File.WriteAllText(Config._path + "\\" + Config._currentSeason.path + "\\blocker.txt", "");


        //    } catch (Exception e)
        //    {
        //        Progress.AddText(e.ToString());
        //    }
        //}

        public static void LoadConfig()
        {

#if DEBUG
            if (DebugOnlyCode)
            {
                Progress.AddText("Starting loadconfig()");
            }
#endif

            string xmlpath = "Config\\";
            string xmlFile = "config.xml";

            // Проверка существования файла
            if ( File.Exists(xmlpath + xmlFile) )
            {
                Progress.AddText($"Loading config from {xmlpath + xmlFile}");
                string xml;
                try
                {
                    xml = File.ReadAllText(xmlpath + xmlFile);
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(xml);
                    Progress.AddText("Config loaded from file");

                    string jsonText = JsonConvert.SerializeXmlNode(doc);
                    JObject config = JObject.Parse(jsonText);

                    string wgkey = (string)config["DiscordServer"]["config"]["wgapi"]["wgkey"];
                    string url = (string)config["DiscordServer"]["config"]["wgapi"]["url"];
                    string realm = (string)config["DiscordServer"]["config"]["wgapi"]["realm"];
                    string path = (string)config["DiscordServer"]["data"]["clans"]["path"];
                    string filename = (string)config["DiscordServer"]["data"]["clans"]["filename"];
                    int currentSeason = (int)config["DiscordServer"]["data"]["clans"]["seasons"];

                    Season season = new Season();
                    season.max_division = (int)config["DiscordServer"]["data"]["clans"]["season"]["max_division"];
                    season.min_division = (int)config["DiscordServer"]["data"]["clans"]["season"]["min_division"];
                    season.max_league = (int)config["DiscordServer"]["data"]["clans"]["season"]["max_league"];
                    season.min_league = (int)config["DiscordServer"]["data"]["clans"]["season"]["min_league"];
                    season.path = (string)config["DiscordServer"]["data"]["clans"]["season"]["path"];

                    Config.LoadConfig(currentSeason, wgkey, url, realm, path, filename, season);


                }
                catch (Exception e)
                {
                    Progress.AddText("Can't load config from file");
                    Progress.AddText(e.ToString());
                }


            } else
            {
                Progress.AddText($"Can't find file {xmlFile}");
            }
            
        }
    }
}
