﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public  class com_ranked : ModuleBase<SocketCommandContext>
    {
        

        public class playerShip
        {
            public string name { get; set; }
            public string type { get; set; }

            public uint id { get; set; }

            public int tier { get; set; }
            public int solo_battles { get; set; }
            public int solo_wins { get; set; }
            public int solo_AWGexp { get; set; }
            public int solo_AWGdmg { get; set; }

            public float solo_AWGplanes { get; set; }
            public float solo_AWGFrags { get; set; }

            public int div2_battles { get; set; }
            public int div2_wins { get; set; }
            public int div2_AWGexp { get; set; }
            public int div2_AWGdmg { get; set; }

            public float div2_AWGplanes { get; set; }
            public float div2_AWGFrags { get; set; }
        }

        [Command("rank")]
        [Alias("rank", "ранг")]
        public async Task RankedAsync(string playerName = "", [Remainder]string additionalInfo = "")
        {
            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            Program.ChatList.ChatConfig varConfig = new Program.ChatList.ChatConfig();

            if (Context.IsPrivate && !Context.User.IsBot)
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);

            }
            else
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
            }

            lang = Program.LangConfig.getLangConfig(varConfig.locale);

            additionalInfo = additionalInfo.ToLower();
            string realm = "eu";
            string locale = "eng";

            // Пробуем найти сохранённые настройки для чата
            Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();
            if (Context.IsPrivate)
            {
                currentConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);
                if (currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    locale = currentConfig.locale;
                }
            }
            else
            {
                currentConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
                if (currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    locale = currentConfig.locale;
                }
            }


            if ( playerName == "" || playerName == "full" )
            {
                if (playerName == "full")
                {
                    additionalInfo += " full";
                }
                playerName = Context.User.Username;
            }

            // Если никнейм с пробелом, удаляем всё после него
            if (playerName.Contains(" "))
            {
                playerName = playerName.Substring(0, playerName.IndexOf(' '));
            }
            //Удаляем все спецсимволы кроме "_"
            playerName = new string((from c in playerName where char.Equals(c, '_') || char.IsLetterOrDigit(c) select c).ToArray());


            if (additionalInfo.Contains("eu"))
            {
                realm = "eu";
            }
            if (additionalInfo.Contains("na"))
            {
                realm = "com";
            }
            if (additionalInfo.Contains("cis"))
            {
                realm = "ru";
            }

            Program.player player = new Program.player();

            player.name = playerName;

            player.id = getPlayerID(playerName, realm);

            if (Regex.Match(additionalInfo, @"\d+").Value == "")
            {
                player.season = Program.ProgramConfig._ranked_season;
            }

            else
            {
                player.season = Int32.Parse(Regex.Match(additionalInfo, @"\d+").Value);
            }

            

            EmbedBuilder builder = new EmbedBuilder();
            player = getPlayerRanks(player.id, realm, player.season);
            if (player.id != 0)
            {
                // Проверка на наличие игрока с таким ником
                // Проверка на скрытую статистику
                if (player.isHidden == true)
                {
                    builder.WithTitle( lang.rankErrTitle )
                        .WithDescription( Program.stRes(lang.rankErrHidden, player.name, getPlayerRealm(realm) ) )
                        .WithColor(Color.DarkGrey);
                }
                else if (!player.isPlayed)
                {
                    // Проверка на игру в сезоне
                    builder.WithTitle( lang.rankErrTitle )
                    .WithDescription( Program.stRes(lang.rankErrEmpty, player.name, getPlayerRealm(realm), player.season.ToString()) )
                    .WithColor(Color.DarkGrey);
                }
                else
                {
                    

                    builder.WithTitle( Program.stRes(lang.rankShowTitle, player.name, getPlayerRealm(realm) ) )
                                    .AddInlineField(lang.rankFieldNickname, $"{getClanName(player.id, realm)}{player.name}")
                                    .AddInlineField(lang.rankFieldRankStars, $"{drawRank(player.rank)} {drawStars(player.rank, player.stars)}")
                                    .AddInlineField(lang.rankFieldAvgEXP, (player.solo_AWGexp * player.solo_battles + player.div2_AWGexp * player.div2_battles) / (player.solo_battles + player.div2_battles))
                                    .AddInlineField(lang.rankFieldSeason, player.season)
                                    .AddInlineField(lang.rankFieldBattles, player.solo_battles + player.div2_battles)
                                    .AddInlineField(lang.rankFieldWins, $"{player.solo_wins + player.div2_wins} ({(player.solo_wins + player.div2_wins) * 100 / (player.solo_battles + player.div2_battles)}%)")
                                    .WithColor(Color.Green);

                }

            } else
            {
                // Игрок не найден
                builder.WithTitle( lang.rankErrTitle )
                .WithDescription( Program.stRes(lang.rankErrPlayerNotFound, player.name, realm) )
                .WithColor(Color.Red);
            }
            
            builder.WithFooter(Program.ProgramConfig._botver);

            if ( additionalInfo.Contains("full") && player.id != 0 )
            {
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();

                    // Полная статистика
                    List<playerShip> playerShips = new List<playerShip>();

                    var rankShipsJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/seasons/shipstats/" +
                        "?application_id=" + Program.ProgramConfig._wgkey +
                        "&account_id=" + player.id +
                        "&season_id=" + player.season);

                    JObject ShipsRSS = JObject.Parse(rankShipsJSONtext);

                    for (int i = 0; i < ShipsRSS["data"][player.id.ToString()].Count(); i++)
                    {
                        playerShip playerShip = new playerShip();

                        playerShip.id = (uint)ShipsRSS["data"][player.id.ToString()][i]["ship_id"];

                        JToken solo = ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"];
                        if (solo.Type != JTokenType.Null)
                        {
                            playerShip.solo_battles = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"]["battles"];
                            playerShip.solo_wins = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"]["wins"];
                            playerShip.solo_AWGexp = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"]["xp"] / playerShip.solo_battles;
                            playerShip.solo_AWGdmg = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"]["damage_dealt"] / playerShip.solo_battles;
                            playerShip.solo_AWGFrags = (float)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"]["frags"] / playerShip.solo_battles;
                            playerShip.solo_AWGplanes = (float)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_solo"]["planes_killed"] / playerShip.solo_battles;

                        }

                        JToken div2 = ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"];
                        if (div2.Type != JTokenType.Null)
                        {
                            playerShip.div2_battles = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"]["battles"];
                            playerShip.div2_wins = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"]["wins"];
                            playerShip.div2_AWGexp = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"]["xp"] / playerShip.div2_battles;
                            playerShip.div2_AWGdmg = (int)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"]["damage_dealt"] / playerShip.div2_battles;
                            playerShip.div2_AWGFrags = (float)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"]["frags"] / playerShip.div2_battles;
                            playerShip.div2_AWGplanes = (float)ShipsRSS["data"][player.id.ToString()][i]["seasons"][player.season.ToString()]["rank_div2"]["planes_killed"] / playerShip.div2_battles;

                        }

                        Program.ShipList.Ship ship = new Program.ShipList.Ship();
                        ship = Program.ShipList.getShip(playerShip.id);

                        // Локализация имён кораблей
                        if (locale == "rus")
                        {
                            
                            playerShip.name = ship.nameRu;

                        } else
                        {
                            playerShip.name = ship.name;
                        }

                        playerShip.tier = ship.tier;
                        playerShip.type = ship.type;

                        playerShips.Add(playerShip);
                    }
                
                    playerShips = playerShips.OrderBy(o => o.tier).ToList();

                    int lastTier = 11;
                    foreach (var item in playerShips)
                    {
                        if (item.solo_battles != 0)
                        {
                            if (lastTier != item.tier)
                            {
                                lastTier = item.tier;
                                builder.AddField(lang.rankFieldTier + " (solo)", lastTier);
                            }

                            string classInfo = "";
                            if (item.type == "AirCarrier")
                            {
                                classInfo = "\n" + lang.rankFieldPlanes + string.Format("{0:0}", item.solo_AWGplanes);
                            }



                            builder.AddField($"**{item.name}**",
                                             lang.rankFieldAvgEXP + $"{item.solo_AWGexp} | " +
                                             lang.rankFieldDamage + $"{item.solo_AWGdmg} | " +
                                             lang.rankFieldFrags + $"{string.Format("{0:N2}", item.solo_AWGFrags)} | " +
                                             lang.rankFieldWr + $": **{item.solo_wins}/{item.solo_battles}** ({item.solo_wins * 100 / item.solo_battles}%)" +
                                             classInfo);

                            if (builder.Fields.Count() >= 24)
                            {
                                builder.WithFooter(lang.rankFooterOverflow);
                                break;
                            }
                        }
                        
                    }
                    lastTier = 11;
                    foreach (var item in playerShips)
                    {
                        if (item.div2_battles != 0)
                        {
                            if (lastTier != item.tier)
                            {
                                lastTier = item.tier;
                                builder.AddField(lang.rankFieldTier + " (div2)", lastTier);
                            }

                            string classInfo = "";
                            if (item.type == "AirCarrier")
                            {
                                classInfo = "\n" + lang.rankFieldPlanes + string.Format("{0:0}", item.solo_AWGplanes);
                            }



                            builder.AddField($"**{item.name}**",
                                             lang.rankFieldAvgEXP + $"{item.div2_AWGexp} | " +
                                             lang.rankFieldDamage + $"{item.div2_AWGdmg} | " +
                                             lang.rankFieldFrags + $"{string.Format("{0:N2}", item.div2_AWGFrags)} | " +
                                             lang.rankFieldWr + $": **{item.div2_wins}/{item.div2_battles}** ({item.div2_wins * 100 / item.div2_battles}%)" +
                                             classInfo);

                            if (builder.Fields.Count() >= 24)
                            {
                                builder.WithFooter(lang.rankFooterOverflow);
                                break;
                            }
                        }
                    }
                }
            }

            await ReplyAsync("", false, builder.Build());
        }


        public static string drawStars(int playerRank, int playerStars)
        {
            int star_nums = 0;
            string output = "";

            // Число звёзд в лиге
            if (playerRank == 23)
            {
                star_nums = 1;
            }
            else if (playerRank >= 16)
            {
                star_nums = 2;
            }
            else if (playerRank >= 11)
            {
                star_nums = 3;
            }
            else if (playerRank >= 6)
            {
                star_nums = 4;
            }
            else if (playerRank >= 2)
            {
                star_nums = 5;
            }
            else
            {
                star_nums = 0;
            }

            for (int i = 0; i < playerStars; i++)
            {
                output += ":star: ";
            }
            for (int i = 0; i < star_nums - playerStars; i++)
            {
                output += ":eight_pointed_black_star: ";
            }
            return (output);
        }

        public static string drawRank(int playerRank)
        {
            if (playerRank <= 15)
            {
                switch (playerRank)
                {
                    case 1:
                        return (":one:");
                    case 2:
                        return (":two:");
                    case 3:
                        return (":three:");
                    case 4:
                        return (":four:");
                    case 5:
                        return (":five:");
                    case 6:
                        return (":six:");
                    case 7:
                        return (":seven:");
                    case 8:
                        return (":eight:");
                    case 9:
                        return (":nine:");
                    case 10:
                        return (":keycap_ten:");
                    case 11:
                        return (":one::one:");
                    case 12:
                        return (":one::two:");
                    case 13:
                        return (":one::three:");
                    case 14:
                        return (":one::four:");
                    case 15:
                        return (":one::five:");
                    case 0:
                    default:
                        return ("-");
                }
            }
            else
            {
                return (playerRank.ToString());
            }

        }

        private string getClanName(uint playerID, string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                //Console.WriteLine("starting fetch clan name");
                WebClient n = new WebClient();
                var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/accountinfo/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + playerID + "&extra=clan" +
                    "&fields=clan.tag%2C+clan.name%2C+clan.clan_id");
                JObject rss = JObject.Parse(clanJSONtext);
                if ((string)rss["status"] == "ok" && rss["data"][playerID.ToString()]["clan"].HasValues == true)
                {
                    //Console.WriteLine("**[" + (string)rss["data"].First.First["clan"]["tag"] + "]** ");
                    return ("**[" + (string)rss["data"].First.First["clan"]["tag"] + "]** ");
                }
                return ("");
            }
        }

        private static int getClanID(int playerID)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var clanJSONtext = n.DownloadString("https://api.worldofwarships.ru/wows/clans/accountinfo/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + playerID + "&extra=clan" +
                    "&fields=clan.tag%2C+clan.name%2C+clan.clan_id");
                //_playerJSON account2 = JsonConvert.DeserializeObject<_playerJSON>(clanJSONtext);
                JObject rss = JObject.Parse(clanJSONtext);
                if ((string)rss["status"] == "ok" && rss["meta"]["data"][playerID.ToString()]["clan"].HasValues == true)
                {
                    // (object)rss["data"].First.First.First == null
                    return (int)rss["data"].First.First["clan"]["clan_id"];
                }
                return 0;
            }
        }

        private static uint getPlayerID(string playerName, string realm) 
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var playerJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/list/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "&search=" + playerName +
                    "&type=exact&limit=1&fields=account_id");

                //Console.WriteLine(playerJSONtext);

                JObject account = JObject.Parse(playerJSONtext);

                if ( (int)account["meta"]["count"] == 1 && (string)account["status"] == "ok")
                {
                    //Console.WriteLine($"account id of user {playerName} on realm {realm} is {(uint)account["data"][0]["account_id"]}");
                    return ( (uint)account["data"][0]["account_id"] );
                }
                return 0;
            }
        }

        public static Program.player getPlayerRanks(uint v, string realm, int season = 9)
        {
            Program.player varplayer = new Program.player();
            if (v != 0)
            {
                //Console.WriteLine($"ID is {v}");
                varplayer.id = v;
                varplayer.season = season;
                varplayer.rank = 0;
                varplayer.name = getPlayerName(v, realm);

            
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();


                    var rankJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/seasons/accountinfo/?" +
                        "application_id=" + Program.ProgramConfig._wgkey +
                        "&account_id=" + varplayer.id +
                        "&season_id=" + season);

                    JObject account = JObject.Parse(rankJSONtext);


                    if ((string)account["status"] == "ok" && varplayer.id != 0)
                    {
                        // Аккаунт найден
                        JToken token = account["meta"]["hidden"];
                        if (token.Type == JTokenType.Null)
                        {
                            //Console.WriteLine("starting load account");
                            if (account["data"][varplayer.id.ToString()].HasValues == true)
                            {
                                varplayer.stars = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_info"]["stars"];
                                varplayer.rank = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_info"]["rank"];

                                // Аккаунт публичен и играл в сезоне
                                JToken solo = account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_solo"];
                                if (solo.Type != JTokenType.Null)
                                {
                                    
                                    varplayer.solo_battles = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_solo"]["battles"];
                                    varplayer.solo_wins = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_solo"]["wins"];
                                    varplayer.solo_AWGexp = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_solo"]["xp"] / varplayer.solo_battles;
                                    varplayer.wr = varplayer.solo_wins * 100 / varplayer.solo_battles;
                                    varplayer.isPlayed = true;
                                }

                                JToken div2 = account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_div2"];
                                if (div2.Type != JTokenType.Null)
                                {
                                    varplayer.div2_battles = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_div2"]["battles"];
                                    varplayer.div2_wins = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_div2"]["wins"];
                                    varplayer.div2_AWGexp = (int)account["data"][varplayer.id.ToString()]["seasons"][varplayer.season.ToString()]["rank_div2"]["xp"] / varplayer.div2_battles;
                                    varplayer.wr = varplayer.div2_wins * 100 / varplayer.div2_battles;
                                    varplayer.isPlayed = true;
                                }


                            }
                            else
                            {
                                varplayer.isPlayed = false;
                            }
                        }
                        else
                        {
                            varplayer.isHidden = true;
                        }
                    }
                }
            }

            //Console.WriteLine(varplayer.id);
            return varplayer;
        }

        private static string getPlayerName(uint v, string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();

                var playerName = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/info/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + v +
                    "&fields=nickname");

                JObject account = JObject.Parse(playerName);

                return ((string)account["data"][v.ToString()]["nickname"]);
            }
        }
        private string getPlayerRealm(string realm)
        {
            if (realm == "ru")
                return "CIS";
            if (realm == "com")
                return "NA";
            return "Europe";
        }
    }
}
