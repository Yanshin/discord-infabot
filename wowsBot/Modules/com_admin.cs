﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class com_admin : ModuleBase<SocketCommandContext>
    {
        [Command("admin")]
        public async Task adminAsync([Remainder] string additionalInfo = "")
        {
            if (Context.User.Id == 371240167198949387)
            {
                additionalInfo = additionalInfo.ToLower();

                if (additionalInfo.Contains("pushcb"))
                {
                    string realm = additionalInfo.Substring(additionalInfo.IndexOf("pushcb") + "pushcb".Length);
                    //wowsBot.

                    //pushClanStats(realm);
                }

                EmbedBuilder builder = new EmbedBuilder();
                builder.WithTitle("Admin info")
                    .AddField("Push complete", "All ")
                    .WithFooter(Program.ProgramConfig._botver)
                    .WithColor(Color.Purple);
                await ReplyAsync("", false, builder.Build());

            } else
            {

            }
        }
    }
}
