﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class com_help : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        [Alias("help", "помощь")]
        public async Task helpAsync()
        {
            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            Program.ChatList.ChatConfig varConfig = new Program.ChatList.ChatConfig();

            if (Context.IsPrivate && !Context.User.IsBot)
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);

            }
            else
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
            }

            lang = Program.LangConfig.getLangConfig(varConfig.locale);

            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle(lang.helpTitle)
                .AddField("-", lang.helpDesc)
                .AddField(lang.helpHelp, lang.helpHelp2)
                .AddField(lang.helpStatus, lang.helpStatus2)
                .AddField(lang.helpRigged, lang.helpRigged2)
                .AddField(lang.helpConfig, lang.helpConfig2)
                .AddField(lang.helpRank, lang.helpRank2)
                .AddField(lang.helpClan, lang.helpClan2)
                .AddField(lang.helpClanRanked, lang.helpClanRanked2)
                .WithFooter(Program.ProgramConfig._botver)
                .WithColor(Color.LighterGrey);
            await ReplyAsync("", false, builder.Build());
        }
    }
}