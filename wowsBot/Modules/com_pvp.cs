﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public  class com_pvp : ModuleBase<SocketCommandContext>
    {
        public class playerShip
        {
            public string name { get; set; }
            public string type { get; set; }

            public uint id { get; set; }

            public int tier { get; set; }
            public int solo_battles { get; set; }
            public int solo_wins { get; set; }
            public int solo_AWGexp { get; set; }
            public int solo_AWGdmg { get; set; }

            public float solo_AWGplanes { get; set; }
            public float solo_AWGFrags { get; set; }

            public int div2_battles { get; set; }
            public int div2_wins { get; set; }
            public int div2_AWGexp { get; set; }
            public int div2_AWGdmg { get; set; }

            public float div2_AWGplanes { get; set; }
            public float div2_AWGFrags { get; set; }
        }

        [Command("pvp")]
        [Alias("pvp", "рандом")]
        public async Task RandomAsync(string playerName = "", [Remainder]string additionalInfo = "")
        {
            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            Program.ChatList.ChatConfig varConfig = new Program.ChatList.ChatConfig();

            if (Context.IsPrivate && !Context.User.IsBot)
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);

            }
            else
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
            }

            lang = Program.LangConfig.getLangConfig(varConfig.locale);

            additionalInfo = additionalInfo.ToLower();
            string realm = "eu";
            string locale = "eng";

            // Пробуем найти сохранённые настройки для чата
            Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();
            if (Context.IsPrivate)
            {
                currentConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);
                if (currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    locale = currentConfig.locale;
                }
            }
            else
            {
                currentConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
                if (currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    locale = currentConfig.locale;
                }
            }


            if ( playerName == "" || playerName == "full" )
            {
                if (playerName == "full")
                {
                    additionalInfo += " full";
                }
                playerName = Context.User.Username;
            }

            // Если никнейм с пробелом, удаляем всё после него
            if (playerName.Contains(" "))
            {
                playerName = playerName.Substring(0, playerName.IndexOf(' '));
            }
            //Удаляем все спецсимволы кроме "_"
            playerName = new string((from c in playerName where char.Equals(c, '_') || char.IsLetterOrDigit(c) select c).ToArray());


            if (additionalInfo.Contains("eu"))
            {
                realm = "eu";
            }
            if (additionalInfo.Contains("na"))
            {
                realm = "com";
            }
            if (additionalInfo.Contains("cis"))
            {
                realm = "ru";
            }


            Program.player player = new Program.player();

            player.name = playerName;

            player.id = getPlayerID(playerName, realm);

            

            EmbedBuilder builder = new EmbedBuilder();
            player = getPlayerRandom(player.id, realm);

            if (player.id != 0)
            {
                // Проверка на наличие игрока с таким ником
                // Проверка на скрытую статистику
                if (player.isHidden == true)
                {
                    builder.WithTitle( "Ошибка получения статистики" )
                        .WithDescription( Program.stRes(lang.rankErrHidden, player.name, getPlayerRealm(realm) ) )
                        .WithColor(Color.DarkGrey);
                }
                else if (!player.isPlayed)
                {
                    // Проверка на игру в указаном диапазоне
                    builder.WithTitle("Ошибка получения статистики")
                    .WithDescription($"Игрок {player.name} на сервере {getPlayerRealm(realm)} не играл в pvp последние сутки" )
                    .WithColor(Color.DarkGrey);
                }
                else
                {
                    

                    builder.WithTitle( "Статистика игрока " + player.name + " " + getPlayerRealm(realm) )
                                    .AddInlineField(lang.rankFieldNickname, $"{getClanName(player.id, realm)}{player.name}")
                                    .AddInlineField("Средний урон",player.solo_AWGdmg)
                                    .AddInlineField("Фраги", player.solo_frags)
                                    .AddInlineField(lang.rankFieldAvgEXP, player.solo_AWGexp)
                                    .AddInlineField(lang.rankFieldBattles, player.solo_battles)
                                    .AddInlineField(lang.rankFieldWins, $"{player.solo_wins} ({player.solo_wins * 100 / player.solo_battles }%)")
                                    .WithColor(Color.Green);
                }

            } else
            {
                // Игрок не найден
                builder.WithTitle( lang.rankErrTitle )
                .WithDescription( Program.stRes(lang.rankErrPlayerNotFound, player.name, realm) )
                .WithColor(Color.Red);
            }
            
            builder.WithFooter(Program.ProgramConfig._botver);

            
            await ReplyAsync("", false, builder.Build());
        }

        private Program.player getPlayerRandom(uint v, string realm, string date = "")
        {
            Program.player varplayer = new Program.player();
            if (v != 0)
            {
                varplayer.id = v;
                varplayer.rank = 0;
                varplayer.name = getPlayerName(v, realm);

                DateTime searchDate = new DateTime();

                searchDate = DateTime.Today;

                searchDate = searchDate.AddDays(-1);

                string DateS = "";
                string PrevDateS = "";
                if (date != "")
                {
                    // сложная функция парсинга введённой даты

                    // когда нибудь будет здесь
                } else
                {
                    DateS = searchDate.ToString("yyyyMMdd");
                    searchDate = searchDate.AddDays(-1);
                    PrevDateS = searchDate.ToString("yyyyMMdd");
                }
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();

                    var randomJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/statsbydate/" +
                        "?application_id=" + Program.ProgramConfig._wgkey +
                        "&account_id=" + varplayer.id +
                        "&dates=" + DateS + "%2C" + PrevDateS);

                    JObject player = JObject.Parse(randomJSONtext);

                    if ((string)player["status"] == "ok" && varplayer.id != 0)
                    {
                        // Аккаунт найден
                        JToken token = player["meta"]["hidden"];
                        if (token.Type == JTokenType.Null)
                        {
                            if (player["data"][varplayer.id.ToString()]["pvp"].HasValues == true)
                            {
                                // Аккаунт публичен

                                JToken UpperStats = player["data"][varplayer.id.ToString()]["pvp"][DateS];
                                JToken BottomStats = player["data"][varplayer.id.ToString()]["pvp"][PrevDateS];

                                varplayer.solo_battles = (int)UpperStats["battles"] - (int)BottomStats["battles"];
                                varplayer.solo_wins = (int)UpperStats["wins"] - (int)BottomStats["wins"];

                                if (varplayer.solo_battles != 0)
                                {
                                    varplayer.solo_AWGexp = ((int)UpperStats["xp"] - (int)BottomStats["xp"]) / varplayer.solo_battles;
                                    varplayer.solo_AWGdmg = ((int)UpperStats["damage_dealt"] - (int)BottomStats["damage_dealt"]) / varplayer.solo_battles;
                                    varplayer.solo_frags = ((int)UpperStats["frags"] - (int)BottomStats["frags"]) / (float)(varplayer.solo_battles);
                                    varplayer.solo_frags = (float)Math.Round(varplayer.solo_frags,2);
                                }

                                varplayer.isPlayed = true;
                            }
                            else
                            {
                                varplayer.isPlayed = false;
                            }
                        }
                        else
                        {
                            varplayer.isHidden = true;
                        }
                    }
                }
            }

            return varplayer;
        }

        
        private string getClanName(uint playerID, string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/accountinfo/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + playerID + "&extra=clan" +
                    "&fields=clan.tag%2C+clan.name%2C+clan.clan_id");
                JObject rss = JObject.Parse(clanJSONtext);
                if ((string)rss["status"] == "ok" && rss["data"][playerID.ToString()]["clan"].HasValues == true)
                {
                    return ("**[" + (string)rss["data"].First.First["clan"]["tag"] + "]** ");
                }
                return ("");
            }
        }

        private static int getClanID(int playerID)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var clanJSONtext = n.DownloadString("https://api.worldofwarships.ru/wows/clans/accountinfo/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + playerID + "&extra=clan" +
                    "&fields=clan.tag%2C+clan.name%2C+clan.clan_id");
                //_playerJSON account2 = JsonConvert.DeserializeObject<_playerJSON>(clanJSONtext);
                JObject rss = JObject.Parse(clanJSONtext);
                if ((string)rss["status"] == "ok" && rss["meta"]["data"][playerID.ToString()]["clan"].HasValues == true)
                {
                    // (object)rss["data"].First.First.First == null
                    return (int)rss["data"].First.First["clan"]["clan_id"];
                }
                return 0;
            }
        }

        private static uint getPlayerID(string playerName, string realm) 
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var playerJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/list/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "&search=" + playerName +
                    "&type=exact&limit=1&fields=account_id");

                JObject account = JObject.Parse(playerJSONtext);
                if ( (int)account["meta"]["count"] == 1 && (string)account["status"] == "ok")
                {
                    return ( (uint)account["data"][0]["account_id"] );
                }
                return 0;
            }
        }

        

        private static string getPlayerName(uint v, string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();

                var playerName = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/info/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + v +
                    "&fields=nickname");

                JObject account = JObject.Parse(playerName);

                return ((string)account["data"][v.ToString()]["nickname"]);
            }
        }
        private string getPlayerRealm(string realm)
        {
            if (realm == "ru")
                return "CIS";
            if (realm == "com")
                return "NA";
            return "Europe";
        }
    }
}
