﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class Com_clan : ModuleBase<SocketCommandContext>
    {
        public class ClanPlayerList
        {
            public List<Program.player> ClanPlayers { set; get; }
        }


        [Command("clan")]
        [Alias("clan", "клан")]
        public async Task ClanAsync(string clanName = "", [Remainder]string additionalInfo = "")
        {
            string playerName = Context.User.Username;

            additionalInfo = additionalInfo.ToLower();

            string realm = "eu";
            string locale = "eng";

            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            

            // Пробуем найти сохранённые настройки для чата
            Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();
            if (Context.IsPrivate)
            {
                currentConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);
                if ( currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    lang = Program.LangConfig.getLangConfig(currentConfig.locale);
                    //locale = currentConfig.locale;
                }
            } else
            {
                currentConfig = Program.ChatList.GetChatConfig( Context.Guild.Id );
                if (currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    lang = Program.LangConfig.getLangConfig(currentConfig.locale);
                    //locale = currentConfig.locale;
                }
            }
            
            int season;
            uint clanID = 0;

            
            //Удаляем все спецсимволы кроме "_"
            clanName = new string((from c in clanName where char.Equals(c, '_') || char.IsLetterOrDigit(c) || char.Equals(c, '-') select c).ToArray());

            
            if (additionalInfo.Contains("eu"))
            {
                realm = "eu";
            }
            if (additionalInfo.Contains("na"))
            {
                realm = "com";
            }
            if (additionalInfo.Contains("cis"))
            {
                realm = "ru";
            }
            if (additionalInfo.Contains("asia"))
            {
                realm = "asia";
            }

            if (Regex.Match(additionalInfo, @"\d+").Value == "")
            {
                season = 9;
            }

            else
            {
                season = Int32.Parse(Regex.Match(additionalInfo, @"\d+").Value);
            }


            EmbedBuilder builder = new EmbedBuilder();
            builder.WithFooter(Program.ProgramConfig._botver);


            // Проверка на наличие дополнительный информации
            if (clanName == "" )
            {
                // Название клана не указано, ищем клан по имени игрока

                // Если никнейм с пробелом, удаляем всё после него
                if (playerName.Contains(" "))
                {
                    playerName = playerName.Substring(0, playerName.IndexOf(' '));
                }

                //Удаляем все спецсимволы
                playerName = new string((from c in playerName
                                         where char.Equals(c, '_') || char.IsLetterOrDigit(c)
                                         select c).ToArray());

                clanID = getClanID(getPlayerID(playerName), realm);

            }
            else
            {
                // Название клана есть в заправшиваемой команде.
                
                clanID = getClanID(0, realm, clanName);
            }

            if ( clanID == 0 )
            {
                // Клан не найден. Выходим из функции
                builder.WithTitle(lang.clanTitleClanInfo)
                    .WithDescription( Program.stRes(lang.clanErrNotFound, clanName) )
                    .WithColor(Color.Red);

                builder.WithFooter(Program.ProgramConfig._botver);

                await ReplyAsync("", false, builder.Build());

                return;
            }

            Program.ClanList.Clan retClan = new Program.ClanList.Clan();
            retClan = Program.ClanList.Display(clanID);


            string correctName = getClanName(clanName, realm);

            // Показ статуса клана в сезоне
            if (correctName != "")
            {
                if (retClan.cID != 0)
                {
                    // Клан участвовал в клановом сезоне
                    builder.WithTitle(lang.clanTitleClanInfo)
                                .AddInlineField(lang.clanName, $"[{retClan.cTag}] ({realm})")
                                .AddInlineField(lang.clanLeague, $"{LeagueName(retClan.cLeague, lang)}")
                                .AddInlineField(lang.clanGroup, $"{retClan.cDivision}")
                                .AddInlineField(lang.clanPosition, $"{retClan.cGlobalRank}")
                                .AddInlineField(lang.clanRating, $"{retClan.cRating}")
                                //.AddInlineField("Глобальная позиция", $"{retClan.cGlobalRank}")
                                .AddInlineField(lang.clanBattles, $"{retClan.cBattles}")
                                .WithColor(Color.Green);
                }
                else
                {
                    // Клан не участвовал в ранговом сезоне
                    builder.WithTitle(lang.clanTitleClanInfo)
                        .WithDescription(Program.stRes(lang.clanErrEmpty, getClanName(clanName, realm).ToString() + " (" + realm + ")"))
                        .WithColor(Color.LightGrey);

                }
            }
            else
            {
                // Клан не участвовал в ранговом сезоне
                builder.WithTitle(lang.clanTitleClanInfo)
                    .WithDescription(Program.stRes(lang.clanErrNotFound, clanName + " (" + realm + ")"))
                    .WithColor(Color.Red);
            }
            await ReplyAsync("", false, builder.Build());
        }

        public static string LeagueName(ushort cLeague, Program.LangConfig.Lang lang)
        {
            switch (cLeague)
            {
                case 0:
                    return lang.clanLeague0;
                case 1:
                    return lang.clanLeague1;
                case 2:
                    return lang.clanLeague2;
                case 3:
                    return lang.clanLeague3;
                case 4:
                    return lang.clanLeague4;
                default:
                    return "";
            }
        }

        public static uint getClanID(uint playerID, string realm, string clanName = "")
        {
            
            if (playerID != 0)
            {
                //Console.WriteLine("Fetchin clanID by playerID");
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();
                    var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/accountinfo/?" +
                        "application_id=" + Program.ProgramConfig._wgkey +
                        "&account_id=" + playerID);
                    JObject rss = JObject.Parse(clanJSONtext);
                    if ( (int)rss["meta"]["count"] == 1 )
                    {
                        //Console.WriteLine($"User ID is {playerID} and ID of clan is {(uint)rss["data"][playerID.ToString()]["clan_id"]}");
                        return ( ( (uint)rss["data"][playerID.ToString()]["clan_id"] ) );
                    }
                    return 0;
                }
            }
            else
            {
                //Console.WriteLine("Fetchin clanID by ClanName");
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();
                    var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/list/" +
                        "?application_id=" + Program.ProgramConfig._wgkey +
                        "&search=" + clanName +
                        "&limit=1");
                    JObject rss = JObject.Parse(clanJSONtext);
                    if ( (string)rss["status"] == "ok" && (int)rss["meta"]["count"] == 1 )
                    {
                        //Console.WriteLine($"clanName is \"{clanName}\" and ID of clan is {(uint)rss["data"][0]["clan_id"]}");
                        return ( (uint)rss["data"][0]["clan_id"] );
                    }
                    return 0;
                }
            }

        }

        private static string getClanName(string clanName = "", string realm="ru")
        {


            //Console.WriteLine("Fetchin clanID by ClanName");
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/list/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&search=" + clanName +
                    "&limit=1");
                JObject rss = JObject.Parse(clanJSONtext);
                if ( (string)rss["status"] == "ok" && (int)rss["meta"]["count"] == 1 )
                {
                    return ( (string)rss["data"][0]["tag"] );
                }
                return "";
            }

        }

        private static uint getPlayerID(string playerName)
        {
            // playerName

            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var playerJSONtext = n.DownloadString("https://api.worldofwarships.ru/wows/account/list/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "search=" + playerName + "&type=exact&limit=1&fields=account_id");
                JObject rss = JObject.Parse(playerJSONtext);
                if ( (string)rss["status"] == "ok" && (int)rss["meta"]["count"] == 1 )
                {
                    return ( (uint)rss["data"][0]["account_id"] );
                }
                return 0;
            }

        }

        private static string getPlayerName(uint v, string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();

                var playerName = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/info/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + v +
                    "&fields=nickname");

                JObject account = JObject.Parse(playerName);

                return ( (string)account["data"][v.ToString()]["nickname"] );
            }
        }

    }
}
