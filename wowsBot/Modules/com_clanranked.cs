﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class Com_clanranked : ModuleBase<SocketCommandContext>
    {
        public class ClanPlayerList
        {
            public List<Program.player> ClanPlayers { set; get; }
        }


        [Command("clanranked")]
        [Alias("clanranked", "кланранг")]
        public async Task ClanAsync(string clanName = "", [Remainder]string additionalInfo = "")
        {
            string playerName = Context.User.Username;

            additionalInfo = additionalInfo.ToLower();

            string realm = "eu";
            string locale = "eng";

            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            

            // Пробуем найти сохранённые настройки для чата
            Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();
            if (Context.IsPrivate)
            {
                currentConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);
                if ( currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    lang = Program.LangConfig.getLangConfig(currentConfig.locale);
                    //locale = currentConfig.locale;
                }
            } else
            {
                currentConfig = Program.ChatList.GetChatConfig( Context.Guild.Id );
                if (currentConfig.chatID != 0)
                {
                    realm = currentConfig.realm;
                    lang = Program.LangConfig.getLangConfig(currentConfig.locale);
                    //locale = currentConfig.locale;
                }
            }
            
            int season;
            uint clanID = 0;

            //Удаляем все спецсимволы кроме "_"
            clanName = new string((from c in clanName where char.Equals(c, '_') || char.IsLetterOrDigit(c) select c).ToArray());

            if (additionalInfo.Contains("eu"))
            {
                realm = "eu";
            }
            if (additionalInfo.Contains("na"))
            {
                realm = "com";
            }
            if (additionalInfo.Contains("cis"))
            {
                realm = "ru";
            }

            if (Regex.Match(additionalInfo, @"\d+").Value == "")
            {
                season = Program.ProgramConfig._ranked_season;
            }

            else
            {
                season = Int32.Parse(Regex.Match(additionalInfo, @"\d+").Value);
            }


            EmbedBuilder builder = new EmbedBuilder();
            builder.WithFooter(Program.ProgramConfig._botver);


            // Проверка на наличие дополнительный информации
            if (clanName == "" )
            {
                // Название клана не указано, ищем клан по имени игрока

                // Если никнейм с пробелом, удаляем всё после него
                if (playerName.Contains(" "))
                {
                    playerName = playerName.Substring(0, playerName.IndexOf(' '));
                }

                //Удаляем все спецсимволы
                playerName = new string((from c in playerName
                                         where char.Equals(c, '_') || char.IsLetterOrDigit(c)
                                         select c).ToArray());

                clanID = getClanID(getPlayerID(playerName), realm);

            }
            else
            {
                // Название клана есть в заправшиваемой команде.
                
                clanID = getClanID(0, realm, clanName);
            }

            if ( clanID == 0 )
            {
                // Клан не найден. Выходим из функции
                builder.WithTitle(lang.clanTitleClanInfo)
                    .WithDescription( Program.stRes(lang.clanErrNotFound, clanName) )
                    .WithColor(Color.Red);

                builder.WithFooter(Program.ProgramConfig._botver);

                await ReplyAsync("", false, builder.Build());

                return;
            }

            Program.ClanList.Clan retClan = new Program.ClanList.Clan();
            retClan = Program.ClanList.Display(clanID);


            string correctName = getClanName(clanName, realm);

            //Console.WriteLine($"Ranked stats of all clan members");
            var loadingEmoji = new Emoji("🔄");
            var TaskCompleteEmoji = new Emoji("✅");
            await Context.Message.AddReactionAsync(loadingEmoji);

            // Создаём список клана
            ClanPlayerList clanPlayerList = new ClanPlayerList();
            clanPlayerList.ClanPlayers = new List<Program.player>();

            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var memberslistJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/info/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&clan_id=" + clanID +
                    "&fields=members_ids");

                JObject memberslistTree = JObject.Parse(memberslistJSONtext);

                JArray memberslist = JArray.FromObject((object)memberslistTree["data"][clanID.ToString()]["members_ids"]);

                builder.WithTitle(Program.stRes(lang.clanRanked, clanName.ToUpper(), season.ToString() ));


                // Попытка в многопоточность
                if (memberslist.Count() > 4)
                {
                    

                    for (int i = 0; i < memberslist.Count / 4; i++)
                    {
                        Program.player playerCounter = new Program.player();
                        Program.player playerCounter2 = new Program.player();
                        Program.player playerCounter3 = new Program.player();
                        Program.player playerCounter4 = new Program.player();
                        Task task1, task2, task3, task4;

                        task1 = Task.Factory.StartNew(() => playerCounter = getPlayerCounter((uint)memberslist[i * 4], realm, season));

                        if (i * 4 + 1 < memberslist.Count)
                        {
                            task2 = Task.Factory.StartNew(() => playerCounter2 = getPlayerCounter((uint)memberslist[i * 4 + 1], realm, season));
                            
                        } else
                        {
                            task2 = Task.Factory.StartNew(() => playerCounter2);
                        }

                        if (i * 4 + 2 < memberslist.Count)
                        {
                            task3 = Task.Factory.StartNew(() => playerCounter3 = getPlayerCounter((uint)memberslist[i * 4 + 2], realm, season));
                        }
                        else
                        {
                            task3 = Task.Factory.StartNew(() => playerCounter3);
                        }

                        if (i * 4 + 3 < memberslist.Count)
                        {
                            task4 = Task.Factory.StartNew(() => playerCounter4 = getPlayerCounter((uint)memberslist[i * 4 + 3], realm, season));
                        }
                        else
                        {
                            task4 = Task.Factory.StartNew(() => playerCounter4);
                        }

                        Task.WaitAll(task1, task2, task3, task4);
                        clanPlayerList.ClanPlayers.Add(playerCounter);
                        clanPlayerList.ClanPlayers.Add(playerCounter2);
                        clanPlayerList.ClanPlayers.Add(playerCounter3);
                        clanPlayerList.ClanPlayers.Add(playerCounter4);

                    }
                } else
                {
                    foreach (var item in memberslist)
                    {
                        Program.player playerCounter = new Program.player();
                        playerCounter = com_ranked.getPlayerRanks((uint)item, realm, season);
                        playerCounter.name = getPlayerName((uint)item, realm);
                        clanPlayerList.ClanPlayers.Add(playerCounter);
                    }
                }

                clanPlayerList.ClanPlayers = clanPlayerList.ClanPlayers.FindAll(o => o.rank!= 0);
                clanPlayerList.ClanPlayers = clanPlayerList.ClanPlayers.OrderBy(o => o.rank).ThenByDescending(o => o.wr).ToList();

                // Цикл вывода информации
                string StrToField = "";

                bool TitleSwitcher = true;
                int lastRank = 0;
                for (int i = 0; i < clanPlayerList.ClanPlayers.Count(); i++)
                {
                    //Console.WriteLine($"{clanPlayerList.ClanPlayers[i].name} {clanPlayerList.ClanPlayers[i].rank} {clanPlayerList.ClanPlayers[i].stars}");

                    

                    if ( i > 0)
                    {
                        int currentIRank = clanPlayerList.ClanPlayers[i].rank;
                        int prefIRank = clanPlayerList.ClanPlayers[i-1].rank;

                        if ( currentIRank != prefIRank || StrToField.Length > 800 || i == clanPlayerList.ClanPlayers.Count() - 1)
                        {
                            if ( prefIRank == 1)
                            {
                                builder.AddField("1", StrToField);
                                StrToField = "";
                            }
                            else if ( prefIRank >= 2 && prefIRank <= 5)
                            {
                                if ( currentIRank >= 6)
                                {
                                    builder.AddField("2-5", StrToField);
                                    StrToField = "";
                                }

                            }
                            else if ( prefIRank >= 6 && prefIRank <= 10)
                            {
                                if ( currentIRank >= 11)
                                {
                                    builder.AddField("6-10", StrToField);
                                    StrToField = "";
                                }
                            }
                            else if ( prefIRank >= 11 && prefIRank <= 15)
                            {
                                if ( currentIRank >= 17)
                                {
                                    builder.AddField("11-16", StrToField);
                                    StrToField = "";
                                }
                            }
                            else if ( prefIRank >= 16 && prefIRank <= 22)
                            {
                                if (currentIRank >= 23)
                                {
                                    builder.AddField("16-22", StrToField);
                                    StrToField = "";
                                }
                            } else
                            {
                                builder.AddField("23", StrToField);
                                StrToField = "";
                            }
                        }
                    }

                    StrToField += $"{com_ranked.drawRank(clanPlayerList.ClanPlayers[i].rank)} **{clanPlayerList.ClanPlayers[i].name}** \n" +
                                      $"{com_ranked.drawStars(clanPlayerList.ClanPlayers[i].rank, clanPlayerList.ClanPlayers[i].stars)}" +
                                      $"WR {clanPlayerList.ClanPlayers[i].wr}% " +
                                      $"(**{clanPlayerList.ClanPlayers[i].solo_wins} / {clanPlayerList.ClanPlayers[i].solo_battles}**) " +
                                      $"| Awg EXP: {clanPlayerList.ClanPlayers[i].solo_AWGexp}\n";

                    /*
                    if (i < clanPlayerList.ClanPlayers.Count() - 1)
                    {
                        if (clanPlayerList.ClanPlayers[i].rank != clanPlayerList.ClanPlayers[i + 1].rank || StrToField.Length > 800)
                        {
                            if( clanPlayerList.ClanPlayers[i].rank == 1)
                            {
                                builder.AddField("1", StrToField);
                                StrToField = "";
                            } else if (clanPlayerList.ClanPlayers[i].rank >= 2 && clanPlayerList.ClanPlayers[i].rank <= 5)
                            {
                                if (clanPlayerList.ClanPlayers[i + 1].rank >= 6)
                                {
                                    builder.AddField("2-5", StrToField);
                                    StrToField = "";
                                }
                                
                            } else if (clanPlayerList.ClanPlayers[i].rank >= 6 && clanPlayerList.ClanPlayers[i].rank <= 10)
                            {
                                if (clanPlayerList.ClanPlayers[i + 1].rank >= 11)
                                {
                                    builder.AddField("6-10", StrToField);
                                    StrToField = "";
                                }
                            } else if (clanPlayerList.ClanPlayers[i].rank >= 11 && clanPlayerList.ClanPlayers[i].rank <= 16)
                            {
                                if (clanPlayerList.ClanPlayers[i + 1].rank >= 17)
                                {
                                    builder.AddField("11-16", StrToField);
                                    StrToField = "";
                                }
                            } else
                            {
                                
                            }

                        }
                    } else
                    {
                        builder.AddField("++", StrToField);
                        StrToField = "";
                    }
                    */
                }
            }
            await Context.Message.AddReactionAsync(TaskCompleteEmoji);
            await ReplyAsync("", false, builder.Build());
        }

        private Program.player getPlayerCounter(uint v, string realm, int season)
        {
            Program.player playerCounter = new Program.player();
            playerCounter = com_ranked.getPlayerRanks((uint)v, realm, season);
            playerCounter.name = getPlayerName((uint)v, realm);

            return playerCounter;
        }

        private string LeagueName(ushort cLeague, Program.LangConfig.Lang lang)
        {
            switch (cLeague)
            {
                case 0:
                    return lang.clanLeague0;
                case 1:
                    return lang.clanLeague1;
                case 2:
                    return lang.clanLeague2;
                case 3:
                    return lang.clanLeague3;
                case 4:
                    return lang.clanLeague4;
                default:
                    return "";
            }
        }

        private static uint getClanID(uint playerID, string realm, string clanName = "")
        {
            
            if (playerID != 0)
            {
                //Console.WriteLine("Fetchin clanID by playerID");
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();
                    var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/accountinfo/?" +
                        "application_id=" + Program.ProgramConfig._wgkey +
                        "&account_id=" + playerID);
                    JObject rss = JObject.Parse(clanJSONtext);
                    if ( (int)rss["meta"]["count"] == 1 )
                    {
                        //Console.WriteLine($"User ID is {playerID} and ID of clan is {(uint)rss["data"][playerID.ToString()]["clan_id"]}");
                        return ( ( (uint)rss["data"][playerID.ToString()]["clan_id"] ) );
                    }
                    return 0;
                }
            }
            else
            {
                //Console.WriteLine("Fetchin clanID by ClanName");
                using (WebClient webClient = new System.Net.WebClient())
                {
                    WebClient n = new WebClient();
                    var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/list/" +
                        "?application_id=" + Program.ProgramConfig._wgkey +
                        "&search=" + clanName +
                        "&limit=1");
                    JObject rss = JObject.Parse(clanJSONtext);
                    if ( (string)rss["status"] == "ok" && (int)rss["meta"]["count"] == 1 )
                    {
                        //Console.WriteLine($"clanName is \"{clanName}\" and ID of clan is {(uint)rss["data"][0]["clan_id"]}");
                        return ( (uint)rss["data"][0]["clan_id"] );
                    }
                    return 0;
                }
            }

        }

        private static string getClanName(string clanName = "", string realm="ru")
        {


            //Console.WriteLine("Fetchin clanID by ClanName");
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var clanJSONtext = n.DownloadString("https://api.worldofwarships." + realm + "/wows/clans/list/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&search=" + clanName +
                    "&limit=1");
                JObject rss = JObject.Parse(clanJSONtext);
                if ( (string)rss["status"] == "ok" && (int)rss["meta"]["count"] == 1 )
                {
                    return ( (string)rss["data"][0]["tag"] );
                }
                return "";
            }

        }

        private static uint getPlayerID(string playerName)
        {
            // playerName

            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();
                var playerJSONtext = n.DownloadString("https://api.worldofwarships.ru/wows/account/list/?" +
                    "application_id=" + Program.ProgramConfig._wgkey +
                    "search=" + playerName + "&type=exact&limit=1&fields=account_id");
                JObject rss = JObject.Parse(playerJSONtext);
                if ( (string)rss["status"] == "ok" && (int)rss["meta"]["count"] == 1 )
                {
                    return ( (uint)rss["data"][0]["account_id"] );
                }
                return 0;
            }

        }

        private static string getPlayerName(uint v, string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();

                var playerName = n.DownloadString("https://api.worldofwarships." + realm + "/wows/account/info/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&account_id=" + v +
                    "&fields=nickname");

                JObject account = JObject.Parse(playerName);

                return ( (string)account["data"][v.ToString()]["nickname"] );
            }
        }

    }
}
