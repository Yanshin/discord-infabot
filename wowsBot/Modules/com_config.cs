﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class com_config : ModuleBase<SocketCommandContext>
    {
        [Command("config")]
        [Alias("config", "настроить")]
        public async Task setupAsync([Remainder]string settingsString = "")
        {
            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            Program.ChatList.ChatConfig varConfig = new Program.ChatList.ChatConfig();

            if (Context.IsPrivate && !Context.User.IsBot)
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);
                
            }
            else
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
            }

            lang = Program.LangConfig.getLangConfig(varConfig.locale);
            
            EmbedBuilder builder = new EmbedBuilder();

            string stalkStr = "";

            if (settingsString.Contains("stalk"))
            {
                
                stalkStr = settingsString.Substring( settingsString.IndexOf("stalk") + "stalk".Length );
                stalkStr = stalkStr.Replace(" ", "").ToUpper();
                
                settingsString = settingsString.Substring(0, settingsString.IndexOf("stalk"));

                Console.WriteLine("Stalk substr:" + stalkStr);
                Console.WriteLine("Settings substr:" + settingsString);
            }


            if (Context.IsPrivate && !Context.User.IsBot )
            {
                // Написали в личку боту
                SetChatConfig(Context.Message.Author.Id, Context.Message.Author.Username, settingsString, true, stalkStr);
                
                System.Threading.Thread.Sleep(50);

                Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();
                currentConfig = Program.ChatList.GetChatConfig( Context.Message.Author.Id );

                lang = Program.LangConfig.getLangConfig(varConfig.locale);

                builder.WithTitle( lang.configSetinChat )
                    .WithDescription( Program.stRes(lang.configResultString, getRealmName(currentConfig.realm, lang)) )
                    .WithColor(Color.LighterGrey)
                    .WithFooter(Program.ProgramConfig._botver);

                Console.WriteLine("looking on clanlist");
                if (currentConfig.clanList != null)
                {
                    Console.WriteLine("Adding substr");
                    string clanList = "";
                    foreach (string item in currentConfig.clanList)
                    {
                        clanList += "[" + item + "] ";
                    }
                    builder.WithDescription(builder.Description + "\nClanList: " + clanList);
                }

                await ReplyAsync("", false, builder.Build());
            }
            else
            {
                // Написали на сервере Дискорда
                if (Context.User.Id == Context.Guild.OwnerId || settingsString == "" )
                {
                    // ... и, при этом, имеют роль администратора
                    SetChatConfig(Context.Guild.Id, Context.Guild.Name, settingsString, false, stalkStr, Context.Message.Channel.Id);
                    System.Threading.Thread.Sleep(10);

                    Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();
                    currentConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);

                    lang = Program.LangConfig.getLangConfig(varConfig.locale);

                    builder.WithTitle( lang.configSetinGuild )
                    .WithDescription( Program.stRes(lang.configResultString, getRealmName(currentConfig.realm, lang)) )
                    .WithColor(Color.LighterGrey)
                    .WithFooter(Program.ProgramConfig._botver);

                    if ( currentConfig.clanList != null )
                    {
                        string clanList = "";
                        foreach (string item in currentConfig.clanList)
                        {
                            clanList += "[" + item + "] ";
                        }
                        builder.WithDescription(builder.Description + "\nClanList: " + clanList);
                    }

                    await ReplyAsync("", false, builder.Build());
                }
                else
                {
                    // ... и, при этом, не(!!!) имеют роль администратора
                    var prohibitedEmoji = new Emoji("🚫");
                    await Context.Message.AddReactionAsync(prohibitedEmoji);
                }
            } 
        }

        private void SetChatConfig(ulong id, string name, string settingsString, bool isuser = false, string stalkStr = "", ulong channel = 0)
        {
            Program.ChatList.ChatConfig currentConfig = new Program.ChatList.ChatConfig();

            settingsString = settingsString.ToLower();

            currentConfig.chatID = id;
            currentConfig.locale = "";
            currentConfig.realm = "";
            currentConfig.name = name;
            currentConfig.isuser = isuser;

            //bool issetted = false;

            // Сервер
            if ( settingsString.Contains("cis"))
            {
                currentConfig.realm = "ru";
                //issetted = true;
            }

            if (settingsString.Contains("na"))
            {
                currentConfig.realm = "com";
                //issetted = true;
            }

            if (settingsString.Contains("eu"))
            {
                currentConfig.realm = "eu";
            }

            // Язык
            if ( settingsString.Contains("ru") || settingsString.Contains("rus") )
            {
                currentConfig.locale = "rus";
            }

            if (settingsString.Contains("eng"))
            {
                currentConfig.locale = "eng";
            }

            if (stalkStr != "")
            {
                if (!isuser && channel != 0)
                {
                    currentConfig.channelID = channel;
                }
                string[] clanNames = stalkStr.Split(',');

                if (currentConfig.clanList == null)
                {
                    currentConfig.clanList = new List<string>();
                }

                foreach (string item in clanNames)
                {
                    currentConfig.clanList.Add(item);
                }
            }

            Program.ChatList.ConfigSave(currentConfig);
        }

        private string getRealmName(string realm, Program.LangConfig.Lang lang)
        {
            if (realm == "ru")
                return lang.configRealmRU;
            if (realm == "com")
                return lang.configRealmNA;
            return lang.configRealmEU;
        }
    }
}
