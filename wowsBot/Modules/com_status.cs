﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class com_status : ModuleBase<SocketCommandContext>
    {
        [Command("статус")]
        [Alias("status", "статус")]
        public async Task statusAsync()
        {
            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            Program.ChatList.ChatConfig varConfig = new Program.ChatList.ChatConfig();

            if (Context.IsPrivate && !Context.User.IsBot)
            {
                varConfig = Program.ChatList.GetChatConfig( Context.Message.Author.Id );

            } else
            {
                varConfig = Program.ChatList.GetChatConfig( Context.Guild.Id );
            }

            lang = Program.LangConfig.getLangConfig(varConfig.locale);


            int onlineRU=0, onlineEU=0, onlineNA=0;
            Task task1 = Task.Factory.StartNew(() => onlineRU = getOnline("ru"));
            Task task2 = Task.Factory.StartNew(() => onlineEU = getOnline("eu"));
            Task task3 = Task.Factory.StartNew(() => onlineNA = getOnline("com"));
            Task.WaitAll(task1, task2, task3);

            EmbedBuilder builder = new EmbedBuilder();
            builder.WithTitle(lang.statusTitle)
                .AddField( "WOWS CIS", Program.stRes(lang.statusOnline, onlineRU.ToString() ) )
                .AddField( "WOWS Europe", Program.stRes(lang.statusOnline, onlineEU.ToString() ) )
                .AddField( "WOWS North America", Program.stRes(lang.statusOnline, onlineNA.ToString() ) )
                .WithFooter(Program.ProgramConfig._botver)
                .WithColor(Color.Purple);
            await ReplyAsync("", false, builder.Build());
        }

        private int getOnline(string realm)
        {
            using (WebClient webClient = new System.Net.WebClient())
            {
                WebClient n = new WebClient();

                var onlineJSONtext = n.DownloadString("https://api.worldoftanks." + realm + "/wgn/servers/info/" +
                    "?application_id=" + Program.ProgramConfig._wgkey +
                    "&game=wows");

                JObject rss = JObject.Parse(onlineJSONtext);

                return (int)rss["data"]["wows"][0]["players_online"];
            }    
        }
    }
}
