﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wowsBot.Modules
{
    public class com_rigged : ModuleBase<SocketCommandContext>
    {
        [Command("подкрутить")]
        [Alias("подкрутить", "rigged")]
        public async Task BalanceAsync([Remainder] string name = "")
        {
            Program.LangConfig.Lang lang = new Program.LangConfig.Lang();
            Program.ChatList.ChatConfig varConfig = new Program.ChatList.ChatConfig();

            if (Context.IsPrivate && !Context.User.IsBot)
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Message.Author.Id);

            }
            else
            {
                varConfig = Program.ChatList.GetChatConfig(Context.Guild.Id);
            }

            lang = Program.LangConfig.getLangConfig(varConfig.locale);

            if (name == "")
            {
                name = Context.User.Mention;
            }
            EmbedBuilder builder = new EmbedBuilder();

            Random rnd = new Random();
            int value = rnd.Next(0, 100);

            if (value > 0 && value <= 20)
            {
                builder.WithTitle(lang.riggedTitle)
                        .WithDescription( Program.stRes(lang.riggedWinsAndDamage, name) )
                        .WithColor(Color.Purple);
            }

            if (value > 20 && value <= 40)
            {
                builder.WithTitle(lang.riggedTitle)
                        .WithDescription( Program.stRes(lang.riggedWinsNoDamage, name) )
                        .WithColor(Color.Green);
            }

            if (value > 40 && value <= 60)
            {
                builder.WithTitle(lang.riggedTitle)
                        .WithDescription( Program.stRes(lang.riggedDefeatAndDamage, name) )
                        .WithColor(Color.Orange);
            }

            if (value > 60 && value <= 80)
            {
                builder.WithTitle(lang.riggedTitle)
                        .WithDescription( Program.stRes(lang.riggedDefeatNoDamage, name) )
                        .WithColor(Color.Red);
            }
            if (value > 80 && value <= 100)
            {
                builder.WithTitle(lang.riggedTitleCancel)
                        .WithDescription( Program.stRes(lang.riggedDefaults, name) )
                        .WithColor(Color.LightGrey);
            }

            builder.WithFooter(Program.ProgramConfig._botver);

            await ReplyAsync("", false, builder.Build());
        }
    }
}
