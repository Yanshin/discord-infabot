﻿using CodeHollow.FeedReader;
using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace wowsBot
{
    public partial class Program
    {
        // Классы
        public static class ProgramConfig
        {
            public static int _season { get; set; }
            public static string _wgkey { get; set; }
            public static ushort _ranked_season { get; set; }
            public static string _url { get; set; }
            public static string _path { get; set; }
            public static string _filename { get; set; }
            public static string _botver { get; set; }
            public static string _dskey { get; set; }
            public static Season _currentSeason { get; set; }

            static ProgramConfig()
            {
                _season = 0;
                _wgkey = "";
                _ranked_season = 10;
                _url = "";
                _path = "";
                _filename = "";
                _currentSeason = new Season();
                _dskey = "";
                _botver = " wows bot v0.5.9";
            }

            public static void LoadConfig(int season, string wgkey, string url, string path, string filename, string dskey, ushort ranked_season, Season currentSeason)
            {
                _season = season;
                _wgkey = wgkey;
                _url = url;
                _path = path;
                _filename = filename;
                _dskey = dskey;
                _currentSeason = currentSeason;
                _ranked_season = ranked_season;
            }
        }

        public static class LangConfig
        {
            public class Lang
            {
                
                public string lang { get; set; }

                public string configSetinChat { get; set; }
                public string configSetinGuild { get; set; }
                public string configResultString { get; set; }
                public string configRealmRU { get; set; }
                public string configRealmEU { get; set; }
                public string configRealmNA { get; set; }
                public string configRealmAsia { get; set; }
                public string configClanStalkTitle { get; set; }

                public string helpTitle { get; set; }
                public string helpDesc { get; set; }
                public string helpHelp { get; set; }
                public string helpHelp2 { get; set; }
                public string helpStatus { get; set; }
                public string helpStatus2 { get; set; }
                public string helpRigged { get; set; }
                public string helpRigged2 { get; set; }
                public string helpRank { get; set; }
                public string helpRank2 { get; set; }
                public string helpClan { get; set; }
                public string helpClan2 { get; set; }
                public string helpClanRanked { get; set; }
                public string helpClanRanked2 { get; set; }

                public string helpConfig { get; set; }
                public string helpConfig2 { get; set; }

                public string statusTitle { get; set; }
                public string statusOnline { get; set; }

                public string rankShowTitle { get; set; }
                public string rankErrTitle { get; set; }
                public string rankErrHidden { get; set; }
                public string rankErrEmpty { get; set; }
                public string rankErrPlayerNotFound { get; set; }
                public string rankFieldNickname { get; set; }
                public string rankFieldRankStars { get; set; }
                public string rankFieldAvgEXP { get; set; }
                public string rankFieldSeason { get; set; }
                public string rankFieldBattles { get; set; }
                public string rankFieldWins { get; set; }
                public string rankFieldTier { get; set; }
                public string rankFieldPlanes { get; set; }
                public string rankFieldDamage { get; set; }
                public string rankFieldFrags { get; set; }
                public string rankFieldWr { get; set; }
                public string rankFooterOverflow { get; set; }

                public string clanTitleClanInfo { get; set; }
                public string clanErrNotFound { get; set; }
                public string clanErrEmpty { get; set; }
                public string clanName { get; set; }
                public string clanLeague { get; set; }
                public string clanGroup { get; set; }
                public string clanPosition { get; set; }
                public string clanRating { get; set; }
                public string clanGlobalRating { get; set; }
                public string clanBattles { get; set; }
                public string clanRanked { get; set; }
                public string clanRankedFieldWR { get; set; }
                public string clanFooterOverflow { get; set; }
                public string clanLeague0 { get; set; }
                public string clanLeague1 { get; set; }
                public string clanLeague2 { get; set; }
                public string clanLeague3 { get; set;}
                public string clanLeague4 { get; set; }

                public string riggedTitle { get; set; }
                public string riggedTitleCancel { get; set; }
                public string riggedWinsAndDamage { get; set; }
                public string riggedWinsNoDamage { get; set; }
                public string riggedDefeatAndDamage { get; set; }
                public string riggedDefeatNoDamage { get; set; }
                public string riggedDefaults { get; set; }
            }

            static List<Lang> LangList;

            static LangConfig()
            {
                LangList = new List<Lang>();
            }

            public static void LoadConfig ( List<Lang> langs)
            {
                LangList.AddRange(langs);
            }

            public static Lang getLangConfig ( string lang = "eng" )
            {
                //Console.WriteLine("starting getLangConfig with param:" + lang);
                Lang tempLang = new Lang();

                foreach (var item in LangList)
                {
                    //Console.WriteLine("//" + item.lang + "//");
                    //Console.WriteLine("//" + item.lang == lang + "//");
                    if ( item.lang == lang)
                    {
                        tempLang = item;
                        return tempLang;
                    }
                }
                //Console.WriteLine("Can't find lang. Loading default");
                foreach (var item in LangList)
                {
                    if (item.lang == "eng")
                    {
                        tempLang = item;
                        return tempLang;
                    }
                }
                return tempLang;
            }


            public static int Count()
            {
                return LangList.Count();
            }
        }

        public static class ChatList
        {
            public class ChatConfig
            {
                public ulong chatID { get; set; }
                public string name { get; set; }
                public string realm { get; set; }
                public string locale { get; set; }
                public bool isuser { get; set; }
                public bool newsUpdate { get; set; }

                public bool stalking { get; set; }
                public List<string> clanList { get; set; }
                public ulong channelID { get; set; }
            }

            static List<ChatConfig> ChatConf;

            static ChatList()
            {
                ChatConf = new List<ChatConfig>();
            }

            public static void ConfigSave( ChatConfig ChatRec )
            {
                bool isnew = true;

                // Цикл поиска ID чата
                for (int i=0; i < ChatConf.Count(); i++)
                {
                    if ( ChatConf[i].chatID == ChatRec.chatID && ChatConf.Count > 0 )
                    {
                        // Перезапись настроек у найденного чата
                        if ( ChatRec.locale != "")
                        {
                            ChatConf[i].locale = ChatRec.locale;
                        }

                        if (ChatRec.realm != "")
                        {
                            ChatConf[i].realm = ChatRec.realm;
                        }

                        ChatConf[i].chatID = ChatRec.chatID;
                        ChatConf[i].clanList = ChatRec.clanList;
                        ChatConf[i].name = ChatRec.name;
                        isnew = false;
                        break;
                    }
                }

                if (isnew)
                {
                    // Добавление новых настроек в чат
                    ChatConf.Add(ChatRec);
                    if (ChatRec.realm == "")
                    {
                        ChatRec.realm = "eu";
                        
                    }
                    if (ChatRec.locale == "eng")
                    {
                        ChatRec.realm = "eng";
                    }
                }

                // Перезапись файла
                try
                {
                    string path = "Data";
                    string fileName = "chatslist.json";

                    string shortpath = path + "\\";

                    File.WriteAllText(shortpath + "backup_" + fileName, GetAllinString());
                    File.WriteAllText(shortpath + fileName, GetAllinString()); 
                }
                catch (Exception e)
                {
                    Console.WriteLine("Can't create json-file clanlist:");
                    Console.WriteLine(e);
                }

            }

            public static void ConfigAllLoad( List<ChatConfig> chatConfigs )
            {
                //Загрузка из файла
                ChatConf.AddRange(chatConfigs);
            }

            public static ChatConfig GetChatConfig(ulong chatID)
            {
                ChatConfig varConfig = new ChatConfig();

                varConfig.locale = "eng";
                varConfig.realm = "eu";

                foreach (var item in ChatConf)
                {
                    if ( item.chatID == chatID)
                    {
                        varConfig = item;
                    }
                }
                return varConfig;
            }

            public static List<ChatConfig> StalkClans(string realm)
            {
                List<ChatConfig> retList = new List<ChatConfig>();
                foreach (var item in ChatConf)
                {
                    if ( item.realm == realm )
                    {
                        retList.Add(item);
                    }
                }

                return retList;
            }

            public static string GetAllinString()
            {
                string jsoner = "";

                foreach (ChatConfig item in ChatConf)
                {
                    jsoner += JsonConvert.SerializeObject(item, Formatting.Indented);
                }

                // Сериализация не прописывает запятые между элементами массив и не помещает всё в квадратные скобки. Исправляем.
                jsoner = "[" + jsoner.Replace("}{", "},{") + "]";
                return jsoner;
            }

            public static int Count()
            {
                return ChatConf.Count();
            }


        }

        public class Season
        {
            public int min_league { get; set; }
            public int max_league { get; set; }
            public int min_division { get; set; }
            public int max_division { get; set; }

            public string path { get; set; }

            public DateTime dayEndCIS { get; set; }
            public DateTime dayEndEU { get; set; }
            public DateTime dayEndNA { get; set; }
            public DateTime dayEndAsia { get; set; }

            public DateTime end_date { get; set; }
        }

        public static class ClanList
        {
            public class Clan
            {
                public uint cID { get; set; }
                public ushort cTeam { get; set; }
                public string cRealm { get; set; }
                public string cName { get; set; }
                public string cTag { get; set; }
                public ushort cBattles { get; set; }
                public ushort cDivision { get; set; }
                public ushort cLeague { get; set; }
                public ushort cRank { get; set; }
                public ushort cGlobalRank { get; set; }
                public ushort cRating { get; set; }
                public ushort cPR { get; set; }
            }

            static List<Clan> _cln; // Static List instance

            static ClanList()
            {
                _cln = new List<Clan>();
            }

            public static void Record(Clan newest)
            {
                _cln.Add(newest);
            }

            public static void ClearRecord(List<Clan> newlist, string realm)
            {
                _cln.RemoveAll(item => item.cRealm == realm);

                if (newlist.Count != 0)
                {
                    _cln.AddRange(newlist);
                }
            }

            public static Clan Display(uint clanID, ushort cTeam = 1)
            {
                Clan displayClan = new Clan();
                for (int i = 0; i < ClanList.Count(); i++)
                {
                    if (_cln[i].cID == clanID )//&& _cln[i].cTeam == cTeam)
                    {
                        displayClan = _cln[i];
                        return displayClan;
                    }
                }
                displayClan.cID = 0;
                return displayClan;
            }

            public static Clan DisplayByTag(string tag, string realm, ushort cTeam = 1)
            {
                Clan displayClan = new Clan();
                for (int i = 0; i < ClanList.Count(); i++)
                {
                    if (_cln[i].cTag.Equals(tag) && _cln[i].cRealm.Equals(realm) ) // && _cln[i].cTeam == cTeam
                    {
                        displayClan = _cln[i];
                        return displayClan;
                    }
                }
                displayClan.cID = 0;
                return displayClan;
            }

            public static int Count()
            {
                return _cln.Count;
            }
        }

        public static class ShipList
        {
            public class Ship
            {
                public uint ship_id { get; set; }
                public int tier { get; set; }

                public string name { get; set; }
                public string nameRu { get; set; }
                public string type { get; set; }
                public string nation { get; set; }

                public bool is_premium { get; set; }
                public bool is_special { get; set; }
            }

            static List<Ship> shipList;

            // Инициализация статики
            static ShipList()
            {
                shipList = new List<Ship>();
            }

            // Метод чистовой записи
            public static void fullRecord(List<Ship> ships)
            {
                shipList.Clear();
                shipList.AddRange(ships);
            }

            // Метод поиска корабля по его id
            public static Ship getShip(uint ship_id)
            {
                Ship returnShip = new Ship();
                foreach (var item in shipList)
                {
                    if (ship_id == item.ship_id)
                    {
                        returnShip = item;
                    }
                }
                return returnShip;
            }
            public static int Count()
            {
                return shipList.Count;
            }
        }

        public class player
        {
            public string name { get; set; }
            public uint id { get; set; }
            public int season { get; set; }
            public int rank { get; set; }
            public int stars { get; set; }
            public float solo_frags { get; set; }
            public int solo_AWGexp { get; set; }
            public int solo_AWGdmg { get; set; }
            public int solo_battles { get; set; }
            public int solo_wins { get; set; }

            public int div2_battles { get; set; }
            public int div2_wins { get; set; }
            public int div2_AWGexp { get; set; }

            public int div3_battles { get; set; }
            public int div3_wins { get; set; }
            public int div3_AWGexp { get; set; }

            public int wr { get; set; }
            public bool isHidden { get; set; }
            public bool isPlayed { get; set; }
        }


        // Функции

        private static void loadConfig()
        {
            string xmlpath = "Config\\";
            string xmlFile = "config.xml";

            // Проверка существования файла
            if (File.Exists(xmlpath + xmlFile))
            {
                Console.WriteLine($"Loading config from {xmlpath + xmlFile}");
                string xml;
                try
                {
                    xml = File.ReadAllText(xmlpath + xmlFile);
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(xml);
                    Console.WriteLine("Config loaded from file");

                    string jsonText = JsonConvert.SerializeXmlNode(doc);
                    JObject config = JObject.Parse(jsonText);

                    Console.WriteLine();

                    string wgkey = (string)config["DiscordServer"]["config"]["wgapi"]["wgkey"];
                    string url = (string)config["DiscordServer"]["config"]["wgapi"]["url"];
                    string path = (string)config["DiscordServer"]["data"]["clans"]["path"];
                    string filename = (string)config["DiscordServer"]["data"]["clans"]["filename"];
                    int currentSeason = (int)config["DiscordServer"]["data"]["clans"]["seasons"];
                    ushort ranked_season = (ushort)config["DiscordServer"]["data"]["ranked"]["season"];
                    string dskey = (string)config["DiscordServer"]["config"]["discord"]["dskey"];

                    Season season = new Season();
                    season.max_division = (int)config["DiscordServer"]["data"]["clans"]["season"]["max_division"];
                    season.min_division = (int)config["DiscordServer"]["data"]["clans"]["season"]["min_division"];
                    season.max_league = (int)config["DiscordServer"]["data"]["clans"]["season"]["max_league"];
                    season.min_league = (int)config["DiscordServer"]["data"]["clans"]["season"]["min_league"];
                    season.path = (string)config["DiscordServer"]["data"]["clans"]["season"]["path"];

                    season.end_date = DateTime.Parse((string)config["DiscordServer"]["data"]["clans"]["season"]["date_end"]);

                    season.dayEndCIS = DateTime.Parse((string)config["DiscordServer"]["data"]["clans"]["season"]["dayEndCIS"]);
                    season.dayEndCIS = season.dayEndCIS.AddMinutes(20);

                    season.dayEndEU = DateTime.Parse((string)config["DiscordServer"]["data"]["clans"]["season"]["dayEndEU"]);
                    season.dayEndEU = season.dayEndEU.AddMinutes(20);

                    season.dayEndNA = DateTime.Parse((string)config["DiscordServer"]["data"]["clans"]["season"]["dayEndNA"]);
                    season.dayEndNA = season.dayEndNA.AddMinutes(20);

                    season.dayEndAsia = DateTime.Parse((string)config["DiscordServer"]["data"]["clans"]["season"]["dayEndAsia"]);
                    season.dayEndAsia = season.dayEndAsia.AddMinutes(20);

                    

                    ProgramConfig.LoadConfig(currentSeason, wgkey, url, path, filename, dskey, ranked_season, season);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Can't load config from file");
                    Console.WriteLine(e);
                }
            }
            else
            {
                Console.WriteLine($"Can't find file {xmlFile}");
            }
        }

        private void loadLocales()
        {
            string path = "Config";
            string fileName = "locales.json";
            string loadedFromFile = "";

            // Проверка существования файла
            if (File.Exists(path + "\\" + fileName))
            {
                Console.WriteLine($"Finded {fileName}");
                try
                {
                    loadedFromFile = File.ReadAllText(path + "\\" + fileName);
                    Console.WriteLine("Locale list loaded");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Can't load locale list from a file:");
                    Console.WriteLine(e);
                }

                if (loadedFromFile != "")
                {
                    LangConfig.LoadConfig(JsonConvert.DeserializeObject<List<LangConfig.Lang>>(loadedFromFile));
                }

                Console.WriteLine($"Finded {LangConfig.Count()} locales in file\n");
            }
            else
            {
                Console.WriteLine($"Can't find {fileName}. Locale list is empty");
            }
        }


        private void loadChatConfig()
        {
            string path = "Data";
            string fileName = "chatslist.json";
            string loadedFromFile = "";

            // Проверка существования файла
            if (File.Exists(path + "\\" + fileName))
            {
                Console.WriteLine($"Finded {fileName}");
                try
                {
                    loadedFromFile = File.ReadAllText(path + "\\" + fileName);
                    Console.WriteLine("Chat list loaded");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Can't load chat list from a file:");
                    Console.WriteLine(e);
                }

                if ( loadedFromFile != "" )
                {
                    ChatList.ConfigAllLoad(JsonConvert.DeserializeObject<List<ChatList.ChatConfig>>(loadedFromFile));
                }

                Console.WriteLine($"Finded {ChatList.Count()} chats settings\n");
            } else
            {
                Console.WriteLine($"Can't find {fileName}. Chatlist is empty");
            } 
        }

        private void loadEnc()
        {
            string path = "Data";
            string fileName = "ships_info.json";
            string loadedFromFile = "";

            bool forceread = true;

            int cnter = 0;

            Console.WriteLine("Trying to load ship list from a file...");
            while (forceread)
            {
                if (cnter >= 3)
                {
                    forceread = false;
                    Console.WriteLine("Starting forceread");
                }


                if (File.Exists(path + "\\" + fileName) || forceread == true)
                {
                    try
                    {
                        loadedFromFile = File.ReadAllText(path + "\\" + fileName);
                        Console.WriteLine("Ship list loaded");

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Can't load clan list from file:");
                        Console.WriteLine(e);
                    }
                    ShipList.fullRecord(JsonConvert.DeserializeObject<List<ShipList.Ship>>(loadedFromFile));

                    Console.WriteLine($"Finded {ShipList.Count()} ships\n");
                    return;
                }
                else
                {
                    Console.WriteLine("Finded block-file. Awaiting...");
                    Thread.Sleep(3000);
                    cnter++;
                }
            }
        }

        private async Task NewsChecker(DateTime lastDate)
        {
            // Функция проверки новостей
            //https://worldofwarships.ru/ru/news/

            var feed = FeedReader.Read("https://worldofwarships.ru/ru/rss/news/");


            if (lastDate.AddSeconds(30.0) >= DateTime.Now)
            {
                // Функция запущена в цикле
                List<ChatList.ChatConfig> currentList = new List<ChatList.ChatConfig>();
                currentList = ChatList.StalkClans("ru");

                bool sender = false;

                foreach (var item in currentList)
                {
                    //LangConfig.Lang lang = new Program.LangConfig.Lang();
                    //lang = LangConfig.getLangConfig(item.locale);

                    if (lastDate != feed.Items.ElementAt(0).PublishingDate)
                    {
                        EmbedBuilder builder = new EmbedBuilder();
                        builder.WithTitle(feed.Items.ElementAt(0).Title)
                            .WithDescription(feed.Items.ElementAt(0).Link)
                            .WithColor(Color.Green)
                            .WithFooter(Program.ProgramConfig._botver);

                        await ((ISocketMessageChannel)_client.GetChannel(item.channelID)).SendMessageAsync("", false, builder);
                    }
                }
            }


            lastDate = DateTime.Parse(feed.Items.ElementAt(0).PublishingDate.ToString());

            // отсчитать N секунд
            // создать таск NewsChecker(lastNews)
            Task task1 = Task.Delay(TimeSpan.FromMinutes(0.5)).ContinueWith((x) => NewsChecker(lastDate));
        }

        private void loadClansStats(string realm = "ru", string time = "23:21:00", bool pushing = false)
        {
            infoUpdate(realm, time);

            string path = ProgramConfig._path + "\\" + ProgramConfig._currentSeason.path;
            string fileName = realm + ProgramConfig._filename;
            string loadedFromFile = "";

            bool forceread = true;

            int cnter = 0;

            
            while (forceread)
            {
                if (cnter >= 3)
                {
                    forceread = false;
                    Console.WriteLine("Starting forceread");
                }


                if (File.Exists(path + "\\" + fileName) || forceread == true)
                {
                    try
                    {
                        loadedFromFile = File.ReadAllText(path + "\\" + fileName);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Can't load clan list from file:");
                        Console.WriteLine(e);
                    }

                    Console.WriteLine($"Finded {JsonConvert.DeserializeObject<List<ClanList.Clan>>(loadedFromFile).Count} clans in realm " + realm);
                    ClanList.ClearRecord(JsonConvert.DeserializeObject<List<ClanList.Clan>>(loadedFromFile), realm);
                    if (pushing)
                    {
                        pushClanStats(realm);
                    }
                    return;
                }
                else
                {
                    Console.WriteLine("Finded block-file. Awaiting...");
                    Thread.Sleep(3000);
                    cnter++;
                }
            }
            
        }

        private async Task pushGLHF(string realm, string timeString)
        {
            List<ChatList.ChatConfig> currentList = new List<ChatList.ChatConfig>();
            currentList = ChatList.StalkClans(realm);

            foreach (var item in currentList)
            {
                EmbedBuilder builder = new EmbedBuilder();
                builder.WithTitle("CB started")
                    .WithDescription("Fair Winds and Following Seas, comrades!")
                    .WithColor(Color.Green)
                    .WithFooter(Program.ProgramConfig._botver);

                await((ISocketMessageChannel)_client.GetChannel(item.channelID)).SendMessageAsync("", false, builder);
            }

        }

        public async Task pushClanStats(string realm, bool force = false)
        {
            if (!force)
            {
                Console.WriteLine("Look for pushing clan stats in chats");

                DayOfWeek today = DateTime.Today.DayOfWeek;
                if (realm != "com")
                {
                    if (today == DayOfWeek.Monday || today == DayOfWeek.Tuesday || today == DayOfWeek.Friday)
                    {
                        Console.WriteLine($"Today is {today}. So not today!");
                        return;
                    }
                }
                else
                {
                    if (today == DayOfWeek.Tuesday || today == DayOfWeek.Wednesday || today == DayOfWeek.Saturday)
                    {
                        Console.WriteLine($"Today is {today}. So not today for NA!");
                        return;
                    }
                }
            }
            else
            {
                Console.WriteLine("Pushing clan stats by admin command");
            }
            
            

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "ClanUpdater.exe",
                    Arguments = realm
                }
            };
            process.Start();
            process.WaitForExit();

            string path = ProgramConfig._path + "\\" + ProgramConfig._currentSeason.path;
            string fileName = realm + ProgramConfig._filename;
            string loadedFromFile = File.ReadAllText(path + "\\" + fileName);

            ClanList.ClearRecord(JsonConvert.DeserializeObject<List<ClanList.Clan>>(loadedFromFile), realm);

            Console.WriteLine("Pushing clan-stats started");

            // Обход всего чатлиста по реалму и выгрузка в чатики итогов
            List<ChatList.ChatConfig> currentList = new List<ChatList.ChatConfig>();
            currentList = ChatList.StalkClans(realm);

            bool sender = false;
            foreach (var item in currentList)
            {
                LangConfig.Lang lang = new Program.LangConfig.Lang();
                lang = LangConfig.getLangConfig(item.locale);

                EmbedBuilder builder = new EmbedBuilder();
                builder.WithTitle(lang.configClanStalkTitle)
                    .WithColor(Color.Green)
                    .WithFooter(Program.ProgramConfig._botver);


                for (int i = 0; i <item.clanList.Count(); i++)
                {
                    ClanList.Clan displayClan = new ClanList.Clan();
                    //ClanList.Clan displayClan2 = new ClanList.Clan();

                    displayClan = ClanList.DisplayByTag(item.clanList[i], realm);
                    //displayClan2 = ClanList.DisplayByTag(item.clanList[i], 2);

                    string ClanResults = "";
                    if (displayClan.cBattles != 0)
                    {
                        
                        ClanResults = "#" + displayClan.cGlobalRank + " | "
                        + wowsBot.Modules.Com_clan.LeagueName(displayClan.cLeague, lang) + ",\t"
                        + $"{lang.clanGroup} {displayClan.cDivision} | "
                        + $"Points: {displayClan.cRating}\n"
                        + $"{lang.clanBattles} {displayClan.cBattles}\t"
                        + $"{lang.clanGlobalRating} **{displayClan.cPR}** ";

                        builder.AddField($"{displayClan.cTag}", ClanResults);
                        sender = true;
                    }

                    /*
                    if (displayClan2.cID != 0)
                    {
                        ClanResults += "\n#" + displayClan.cGlobalRank + " (t2)| " + displayClan.cLeague + ", " + displayClan.cDivision + " " + displayClan.cRating + " points. Battles: " + displayClan.cBattles;
                    }*/
  
                }
                if (sender)
                    if (item.isuser)
                    {
                        await sendClanStats(item.chatID, builder);
                    } else
                    {
                        if (item.channelID != 0)
                        {
                            await sendClanStats(item.channelID, builder);
                        } else
                        {
                            await sendClanStats(item.chatID, builder);
                        }
                        
                    }
            }
            Console.WriteLine("Pushing clanstats complete");
        }

        async Task sendClanStats(ulong chatID, EmbedBuilder builder)
        {
            await ((ISocketMessageChannel)_client.GetChannel(chatID)).SendMessageAsync("", false, builder);
        }

        public static string nameEdit(string playerName)
        {
            // Если никнейм с пробелом, удаляем всё после него
            if (playerName.Contains(" "))
            {
                playerName = playerName.Substring(0, playerName.IndexOf(' '));
            }
            //Удаляем все спецсимволы кроме "_"
            playerName = new string((from c in playerName where char.Equals(c, '_') || char.IsLetterOrDigit(c) select c).ToArray());

            return playerName;
        }

        public static string stRes(string localeString, string args = "", string args2 = "", string args3 = "")
        {
            if ( localeString.Contains("%v%") )
            {
                localeString = localeString.Replace("%v%", args);
            }

            if ( localeString.Contains("%v2%") || args2 !="" )
            {
                localeString = localeString.Replace("%v2%", args2);
            }

            if (localeString.Contains("%v3%") || args3 != "")
            {
                localeString = localeString.Replace("%v3%", args3);
            }
            return localeString;
        }
    }
}
