﻿using Microsoft.Extensions.DependencyInjection;

using System;
using System.IO;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace wowsBot
{
    public partial class Program
    {
        static void Main(string[] args) => new Program().RunBotAsync().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        public async Task RunBotAsync()
        {

            _client = new DiscordSocketClient();
            _commands = new CommandService();

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            loadConfig();
            loadLocales();
            loadChatConfig();
            loadEnc();
            NewsChecker(DateTime.Now);

            if (ProgramConfig._currentSeason.end_date >= DateTime.Today)
            {
                Console.WriteLine("Trying to load clan lists from a files...");

                var dateNow = DateTime.Now;
                string timeString;

                var datePlus = dateNow.AddSeconds(30);
                timeString = dateNow.Hour + ":" + dateNow.Minute + ":" + dateNow.Second;

                string timeString2 = datePlus.Hour + ":" + datePlus.Minute + ":" + datePlus.Second;

                //Console.WriteLine(timeString);

                //delayer(pushGLHF("ru", timeString), "ru", timeString);

                loadClansStats("ru", $"{ProgramConfig._currentSeason.dayEndCIS.Hour}:{ProgramConfig._currentSeason.dayEndCIS.Minute}:{ProgramConfig._currentSeason.dayEndCIS.Second}");
                loadClansStats("eu", $"{ProgramConfig._currentSeason.dayEndEU.Hour}:{ProgramConfig._currentSeason.dayEndEU.Minute}:{ProgramConfig._currentSeason.dayEndEU.Second}");
                loadClansStats("com", $"{ProgramConfig._currentSeason.dayEndNA.Hour}:{ProgramConfig._currentSeason.dayEndNA.Minute}:{ProgramConfig._currentSeason.dayEndNA.Second}");
                loadClansStats("asia", $"{ProgramConfig._currentSeason.dayEndAsia.Hour}:{ProgramConfig._currentSeason.dayEndAsia.Minute}:{ProgramConfig._currentSeason.dayEndAsia.Second}");

            } else
            {
                Console.WriteLine("clan wars closed");
            }


            _client.Log += Log;


            await RegisterCommandsAsync();
            await _client.LoginAsync(TokenType.Bot, ProgramConfig._dskey);
            await _client.StartAsync();
            await Task.Delay(-1);

            
        }

        

        private async Task delayer( Task task, string realm, string time)
        {
            // Запуск функции в указанное время скрипта 
            var DailyTime = time;
            var timeParts = DailyTime.Split(new char[1] { ':' });

            var dateNow = DateTime.Now;
            var date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day,
                       int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2]));
            TimeSpan ts;
            if (date > dateNow)
                ts = date - dateNow;
            else
            {
                date = date.AddDays(1);
                ts = date - dateNow;
            }

            //waits certan time and run the code
            Task task1 = Task.Delay(ts).ContinueWith((x) => task );
        }

        private async Task infoUpdate(string realm, string time = "23:21:00")
        {
            // Запуск функции в указанное время скрипта 
            var DailyTime = time;
            var timeParts = DailyTime.Split(new char[1] { ':' });

            var dateNow = DateTime.Now;
            var date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day,
                       int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2]));
            TimeSpan ts;
            if (date > dateNow)
                ts = date - dateNow;
            else
            {
                date = date.AddDays(1);
                ts = date - dateNow;
            }

            //waits certan time and run the code
            Console.WriteLine($"Setting Shedule to reload {realm} clans at {DailyTime}...");
            Task task1 = Task.Delay(ts).ContinueWith((x) => loadClansStats(realm, time, true));
            
        }

        private Task Log(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;

            if (message is null || message.Author.IsBot) return;

            int argPos = 0;

            if (message.HasStringPrefix("/ib ", ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var context = new SocketCommandContext(_client, message);

                var result = await _commands.ExecuteAsync(context, argPos, _services);

                if (!result.IsSuccess)
                {
                    Console.WriteLine(result.ErrorReason);
                    var errorEmoji = new Emoji("🤔");
                    await context.Message.AddReactionAsync(errorEmoji);
                }

            }
        }
    }
}
